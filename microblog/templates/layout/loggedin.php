<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    
    <?= $this->Html->css('login.css') ?>
    <?= $this->Html->css('milligram.min.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('more.css') ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
  <div class="navbar">
    <span>MicroBlog 4.X</span>
    <div class="topnav-right">
      <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-fw fa-home']) . 'Home', ['controller' => 'Posts', 'action' => 'index'], ['escape' => false]) ?>
      <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-sign-out']) . 'Log out', ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]) ?>

    </div>
  </div>
  <main class="main">
    <div >
      <div id='flash' class="flashStyle"><?= $this->Flash->render() ?></div>
      <?= $this->fetch('content') ?>
    </div>
  </main>
</body>
</html>

<script type="text/javascript">
  window.setTimeout("document.getElementById('flash').style.display='none';", 6000); 
</script>
<?= $this->Html->script('index.js') ?>
