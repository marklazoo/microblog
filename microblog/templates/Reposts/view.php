<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Repost $repost
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Repost'), ['action' => 'edit', $repost->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Repost'), ['action' => 'delete', $repost->id], ['confirm' => __('Are you sure you want to delete # {0}?', $repost->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Reposts'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Repost'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="reposts view content">
            <h3><?= h($repost->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $repost->has('user') ? $this->Html->link($repost->user->id, ['controller' => 'Users', 'action' => 'view', $repost->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Post') ?></th>
                    <td><?= $repost->has('post') ? $this->Html->link($repost->post->id, ['controller' => 'Posts', 'action' => 'view', $repost->post->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($repost->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Repost Status Del') ?></th>
                    <td><?= $this->Number->format($repost->repost_status_del) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($repost->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($repost->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
