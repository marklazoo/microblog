<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Repost[]|\Cake\Collection\CollectionInterface $reposts
 */
?>
<div class="reposts index content">
    <?= $this->Html->link(__('New Repost'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Reposts') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('post_id') ?></th>
                    <th><?= $this->Paginator->sort('repost_status_del') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($reposts as $repost): ?>
                <tr>
                    <td><?= $this->Number->format($repost->id) ?></td>
                    <td><?= $repost->has('user') ? $this->Html->link($repost->user->id, ['controller' => 'Users', 'action' => 'view', $repost->user->id]) : '' ?></td>
                    <td><?= $repost->has('post') ? $this->Html->link($repost->post->id, ['controller' => 'Posts', 'action' => 'view', $repost->post->id]) : '' ?></td>
                    <td><?= $this->Number->format($repost->repost_status_del) ?></td>
                    <td><?= h($repost->created) ?></td>
                    <td><?= h($repost->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $repost->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $repost->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $repost->id], ['confirm' => __('Are you sure you want to delete # {0}?', $repost->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
