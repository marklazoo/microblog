<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?= $this->Html->css('password.css') ?>
<div class="registerContent">
    <div class="users form"> <?= $this->Flash->render() ?>
    <center>
        <table>
            <tr>
                <td>
                    <?= $this->Form->create($user) ?>
                    <fieldset>
                        <legend>REGISTER</legend>
                        <?php
                            echo $this->Form->control('username', ['label' => false, 'placeholder' => 'Username...']);
                            echo $this->Form->control('full_name', ['label' => false, 'placeholder' => 'Full name...']);
                            echo $this->Form->control('birthdate', ['label' => false, 'placeholder' => 'Birthdate...',
                                'max' => date('Y-m-d', strtotime(date('Y-m-d') . ' -10 years'))]);
                            echo $this->Form->control('email', ['label' => false, 'placeholder' => 'Email...']);
                            echo $this->Form->control('password', ['label' => false, 'placeholder' => 'Password...',
                                'id' => 'psw']);
                            echo $this->Form->control(
                                'activation_code',
                                ['type' => 'hidden', 'value' => $activation]
                            );
                            ?>
                        <div id="message">
                            Password must contain the following:
                            <table>
                                <tr>
                                    <td>
                                        <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
                                    </td>
                                    <td>
                                        <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p id="number" class="invalid">A <b>number</b></p>
                                    </td>
                                    <td>
                                        <p id="length" class="invalid">Minimum <b>8 characters</b></p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                    <?= $this->Form->button(__('Sign Up')) ?>
                    <?= $this->Form->end() ?>
                    <center>
                        <?= $this->Html->link(
                            'Activate your account?',
                            ['action' => 'activate'],
                            ['class' => 'link']
                        ) ?>
                    </center>
                </td>
            </tr>
        </table>          
        <?= $this->Html->link('Log in', ['action' => 'login'], ['class' => 'register']) ?><br>
        <?= $this->Html->link('Forgot your password?', ['action' => 'forgotpassword'], ['class' => 'link']) ?>
    </div>
</div>
<?= $this->Html->script('password.js') ?>