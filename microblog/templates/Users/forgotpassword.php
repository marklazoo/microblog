<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="forgotPassContent">
    <div class="users form"> <?= $this->Flash->render() ?>
    <center>
        <table>
            <tr>
                <td>
                    <?= $this->Form->create($user) ?>
                    <fieldset>
                        <legend>RESET PASSWORD</legend>
                        <?php
                            echo $this->Form->control('email', ['label' => false, 'placeholder' => 'Email...']);
                            echo '<center>A temperary password will be sent to your email.</center>';
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Reset')) ?>
                    <?= $this->Form->end() ?>
                    <center><?= $this->Html->link(
                        'Already have an account?',
                        ['action' => 'login'],
                        ['class' => 'link']
                    ) ?>
                </td>
            </tr>
        </table>         
        <?= $this->Html->link('Register here', ['action' => 'add'], ['class' => 'register']) ?><br>
        <?= $this->Html->link('Activate your account?', ['action' => 'activate'], ['class' => 'link']) ?>
    </center>
    </div>
</div>
