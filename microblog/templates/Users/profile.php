<?php
    /**
     * @var \App\View\AppView $this
     * @var \App\Model\Entity\User $user
     */
    $this->layout = 'loggedin';
?>
<?= $this->Html->css('password.css') ?>
<div class="editProfileContent">
    <div class="users form">
        <center>
        <?php
        if ($user->profile_pic != null) :
            echo $this->Html->image(
                'profile_pic/' . h($user->profile_pic),
                ['alt' => 'CakePHP', 'border' => '0', 'height' => '200px']
            );
        endif;
        ?>
        </center>
        <table>
            <tr>
                <td style="text-align: right;">
                    <?php echo $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-warning']) . ' Deactivate',
                        ['action' => 'deactivate', h($user->id)],
                        ['escape' => false,
                                'confirm' => __(
                                    'Are you sure you want to deactivate # {0}?',
                                    h($user->id)
                                ),
                        'class' => 'deactivate']
                    ); ?>
                </td>
            </tr>
        </table>
        <?= $this->Form->create($user, ['enctype' => 'multipart/form-data']) ?>
        <fieldset>
            <?php
                echo $this->Form->control('username');
                echo $this->Form->control('full_name');
                echo $this->Form->control(
                    'birthdate',
                    ['max' => date('Y-m-d', strtotime(date('Y-m-d') . ' -10 years'))]
                );
                echo $this->Form->control('newPassword', ['type' => 'password', 'id' => 'psw']);
                ?>
            <div id="message">
                Password must contain the following:
                <table>
                    <tr>
                        <td>
                            <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
                        </td>
                        <td>
                            <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
                        </td>
                    </tr>
                        <tr>
                            <td>
                                <p id="number" class="invalid">A <b>number</b></p>
                            </td>
                            <td>
                                <p id="length" class="invalid">Minimum <b>8 characters</b></p>
                            </td>
                        </tr>
                </table>
            </div>
            <?php
                echo $this->Form->control('confirmPassword', ['type' => 'password']);
                echo $this->Form->control('profile_pic', ['type' => 'file', 'label' => false]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Save Changes')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
<?= $this->Html->script('password.js') ?>
