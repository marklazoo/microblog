<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="loginContent">
    <div class="users form"> <?= $this->Flash->render() ?>
    <center>
        <table>
            <tr>
                <td>
                    <legend>ACTIVATE</legend>
                    <?= $this->Form->create($user) ?>
                    <fieldset>
                        <?php
                            echo $this->Form->control('email', ['label' => false, 'placeholder' => 'Email...']);
                            echo $this->Form->control(
                                'activation_code',
                                ['label' => false, 'placeholder' => 'Activation code...']
                            );
                            ?>
                    </fieldset>
                    <?php echo $this->Form->submit('Activate', ['name' => 'activate']); ?>
                    <?php echo $this->Form->submit('Resend', ['name' => 'resend']); ?>
                    
                    <?= $this->Form->end() ?>
                    <center><?= $this->Html->link('Register here', ['action' => 'add'], ['class' => 'link']) ?></center>
                </td>
            </tr>
        </table>       
        <?= $this->Html->link('Log in', ['action' => 'login'], ['class' => 'register']) ?><br>
        <?= $this->Html->link('Forgot your password?', ['action' => 'forgotpassword'], ['class' => 'link']) ?>
    </center>
    </div>
</div>
