<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="loginContent">
        <div class="users form"> <?= $this->Flash->render() ?>
		<center>
			<table>
				<tr>
					<td>
						<legend>LOGIN</legend>
						<?= $this->Form->create() ?>
						<fieldset>
						<?= $this->Form->control('email', ['required' => true, 'label' => false, 'placeholder' => 'Email...']) ?>
						<?= $this->Form->control('password', ['required' => true, 'label' => false, 'placeholder' => 'Password...']) ?>
						</fieldset>
						<?= $this->Form->submit(__('Login')); ?>
						<?= $this->Form->end() ?>
						<center><?= $this->Html->link('Forgot your password?', ['action' => 'forgotpassword'], ['class' => 'link']) ?></center>
					</td>
				</tr>
			</table>
						
			<?= $this->Html->link('Register here', ['action' => 'add'], ['class' => 'register']) ?><br>
			<?= $this->Html->link('Activate your account?', ['action' => 'activate'], ['class' => 'link']) ?>
			
		</center>
		</div>
	</div>
</body>
</html>