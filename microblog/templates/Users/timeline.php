<?php
    /**
     * @var \App\View\AppView $this
     * @var \App\Model\Entity\Post[]|\Cake\Collection\CollectionInterface $posts
     */
    $this->layout = 'loggedin';
    $uid = $this->request->getAttribute('identity')->getIdentifier();
    $loadCounter = count($timelineMerged);
?>
<div class="row">
    <aside class="column">
        <div class="side-nav content" style="height: 155px; position:relative;">
            <table style="font-size: 13px;">
                <?php foreach ($profile as $profiles) : ?>
                <tr>
                    <td width=30%>
                        <?php if (h($profiles->profile_pic) == null) :
                            echo $this->Html->image(
                                'default.png',
                                ['alt' => 'CakePHP', 'border' => '0', 'height' => '50px', 'width' => '50px']
                            );
                        else :
                            echo $this->Html->image(
                                'profile_pic/' . h($profiles->profile_pic),
                                ['alt' => 'CakePHP', 'border' => '0', 'height' => '50px', 'width' => '50px']
                            );
                        endif; ?>
                    </td>
                    <td>
                        <?php
                            echo '<b>' . h($profiles->full_name) . '</b><br>';
                            echo h($profiles->username);
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php
            if ($uid == h($profiles->id)) :
                echo $this->Html->link(
                    __('Edit Profile'),
                    ['controller' => 'users', 'action' => 'profile', h($profiles->id)],
                    ['class' => 'editProfile']
                );
            elseif ($countCheckFollow != 1) :
                    echo $this->Form->postLink(
                        __('Follow'),
                        ['controller' => 'follows', 'action' => 'timelinefollow', h($profiles->id), h($profiles->id)],
                        ['confirm' => __(
                            'Are you sure you want to follow {0}?',
                            h($profiles->username)
                        ), 'class' => 'editProfile']
                    );
            else :
                    echo $this->Form->postLink(
                        __('Unfollow'),
                        ['controller' => 'follows', 'action' => 'timelineunfollow', h($profiles->id), h($profiles->id)],
                        ['confirm' => __(
                            'Are you sure you want to unfollow {0}?',
                            h($profiles->username)
                        ), 'class' => 'editProfile']
                    );
            endif;
            ?>
        </div>
    </aside>
    <div class="column-responsive column-60">
        <div class="posts form content" style="height: 1000px; position:relative;">
            <?php if ($loadCounter != 0) : ?>
                <div style="height: 100%; position:relative;">
                    <div style="max-height:100%; overflow:auto;">
                        <div  class="list-timeline">
                            <?php foreach ($timelineMerged as $all) : ?>
                                <div class="list-timeline-element">
                                    <table>
                                        <tr>
                                            <td width=20%>
                                                <?php if (h($all['image']) != null) :
                                                    echo $this->Html->image(
                                                        'post_image/' . h($all['image']),
                                                        ['alt' => 'CakePHP', 'border' => '0',
                                                        'height' => '140px', 'width' => '140px']
                                                    );
                                                else :
                                                    echo $this->Html->image(
                                                        'noimage.jpg',
                                                        ['alt' => 'CakePHP', 'border' => '0',
                                                        'height' => '140px', 'width' => '140px']
                                                    );
                                                endif; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (h($all['classification']) == 'post') :
                                                    echo '<b>Authored...</b><br>';
                                                    echo h($all['content']) . '<br>';
                                                    echo h($all['modified']);
                                                    echo '<br>'.$this->Html->link(
                                                        $this->Html->tag('i', '', ['class' => 'fa fa-arrow-circle-right',
                                                        'style' => 'font-size: 150%;']) . ' ',
                                                        ['controller' => 'posts', 'action' => 'view',
                                                        h($all['post_id'])],
                                                        ['escape' => false]
                                                    );
                                                else :
                                                        echo '<b>Shared...</b><br>';
                                                        echo h($all['content']) . '<br>';
                                                        echo h($all['modified']).'<br>';
                                                    if (h($all['repost_user_id']) == $uid) :
                                                        echo $this->Form->postLink(
                                                            $this->Html->tag('i', '', ['class' => 'fa fa-trash', 
                                                                'style' => 'font-size: 150%;']).' ',
                                                            ['controller' => 'reposts', 'action' => 'delete',
                                                            h($all['repost_id'])],
                                                            ['escape' => false,
                                                            'confirm' => __('Are you sure you want to delete this?')]
                                                        );
                                                    endif;
                                                        echo $this->Html->link(
                                                            $this->Html->tag('i', '', ['class' => 'fa fa-arrow-circle-right',
                                                            'style' => 'font-size: 150%;']) . ' ',
                                                            ['controller' => 'posts', 'action' => 'view',
                                                            h($all['post_id'])],
                                                            ['escape' => false]
                                                        );
                                                endif;
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <button onclick='loadmoreTimeline()' id="loadmoreTimeline">Load More...</button>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <aside class="column">
        <div class="side-nav content" style="height: 1000px; position:relative;">
            <div style="height: 42%; position:relative;">
                <h4 class="heading"><?= __('Following') ?></h4>
                <div style="max-height:100%; overflow:auto;">
                    <table style="font-size: 13px;">
                        <?php foreach ($followedUnfollowing as $followedUnfollowings) : ?>
                        <tr>
                            <td width=30%>
                                <?php if (h($followedUnfollowings->profile_pic) == null) :
                                    echo $this->Html->image(
                                        'default.png',
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                else :
                                    echo $this->Html->image(
                                        'profile_pic/' . h($followedUnfollowings->profile_pic),
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                endif; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php
                                if (h($followedUnfollowings->acc_status_del) == 1) :
                                    echo '<b>' . $this->Form->postLink(
                                        __(h($followedUnfollowings->full_name)),
                                        ['controller' => 'users', 'action' => 'timeline', h($followedUnfollowings->id)],
                                        ['confirm' => __(
                                            'Are you sure you want to visit {0}?',
                                            h($followedUnfollowings->username)
                                        ),
                                        'class' => 'side-nav-item']
                                    ) . '</b>';
                                    echo h($followedUnfollowings->username);

                                    if (h($followedUnfollowings->id) != $uid) :
                                        echo $this->Form->postLink(
                                            __('Follow'),
                                            ['controller' => 'follows', 'action' => 'timelinefollow',
                                            h($followedUnfollowings->id), h($profiles->id)],
                                            ['confirm' => __(
                                                'Are you sure you want to follow {0}?',
                                                h($followedUnfollowings->username)
                                            ),
                                            'class' => 'follow']
                                        );
                                    endif;
                                elseif (h($followedUnfollowings->acc_status_del) == 2) :
                                        echo '<b>' . h($followedUnfollowings->full_name) . '</b><br>';
                                        echo '(deactivated)<br>';
                                        echo h($followedUnfollowings->username);
                                endif;

                                ?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </table>
                        <table style="font-size: 13px;">
                            <?php foreach ($followedFollowing as $followedFollowings) : ?>
                            <tr>
                                <td width=30%>
                                    <?php if (h($followedFollowings->profile_pic) == null) :
                                        echo $this->Html->image(
                                            'default.png',
                                            ['alt' => 'CakePHP', 'border' => '0',
                                            'height' => '50px',
                                            'width' => '50px']
                                        );
                                    else :
                                        echo $this->Html->image(
                                            'profile_pic/' . h($followedFollowings->profile_pic),
                                            ['alt' => 'CakePHP', 'border' => '0',
                                            'height' => '50px',
                                            'width' => '50px']
                                        );
                                    endif; ?>
                                </td>
                                <td style="text-align: center;">
                                    <?php
                                    if (h($followedFollowings->acc_status_del) == 1) :
                                        echo '<b>' . $this->Form->postLink(
                                            __(h($followedFollowings->full_name)),
                                            ['controller' => 'users', 'action' => 'timeline',
                                            h($followedFollowings->id)],
                                            ['confirm' => __(
                                                'Are you sure you want to visit {0}?',
                                                h($followedFollowings->username)
                                            ),
                                            'class' => 'side-nav-item']
                                        ) . '</b>';
                                    elseif (h($followedFollowings->acc_status_del) == 2) :
                                        echo '<b>' . h($followedFollowings->full_name) . '</b><br>';
                                        echo '(deactivated)<br>';
                                    endif;

                                        echo h($followedFollowings->username);
                                        echo $this->Form->postLink(
                                            __('Unfollow'),
                                            ['controller' => 'follows', 'action' => 'timelineunfollow',
                                            h($followedFollowings->id), h($profiles->id)],
                                            ['confirm' => __(
                                                'Are you sure you want to unfollow {0}?',
                                                h($followedFollowings->username)
                                            ),
                                            'class' => 'follow']
                                        );
                                    ?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </table>
                </div>
            </div>
            <br><br>
            <br>
            <div style="height: 42%; position:relative;">
                <h4 class="heading"><?= __('Followers') ?></h4>
                <div style="max-height:100%; overflow:auto;">
                    <table style="font-size: 13px;">
                        <?php foreach ($followerUnfollowing as $followerUnfollowings) : ?>
                        <tr>
                            <td width=30%>
                                <?php if (h($followerUnfollowings->profile_pic) == null) :
                                    echo $this->Html->image(
                                        'default.png',
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                else :
                                    echo $this->Html->image(
                                        'profile_pic/' . h($followerUnfollowings->profile_pic),
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                endif; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php
                                if (h($followerUnfollowings->acc_status_del) == 1) :
                                    echo '<b>' . $this->Form->postLink(
                                        __(h($followerUnfollowings->full_name)),
                                        ['controller' => 'users', 'action' => 'timeline', h($followerUnfollowings->id)],
                                        ['confirm' => __(
                                            'Are you sure you want to visit {0}?',
                                            h($followerUnfollowings->username)
                                        ),
                                        'class' => 'side-nav-item']
                                    ) . '</b>';
                                    echo h($followerUnfollowings->username);

                                    if (h($followerUnfollowings->id) != $uid) :
                                        echo $this->Form->postLink(
                                            __('Follow'),
                                            ['controller' => 'follows', 'action' => 'timelinefollow',
                                            h($followerUnfollowings->id), h($profiles->id)],
                                            ['confirm' => __(
                                                'Are you sure you want to follow {0}?',
                                                h($followerUnfollowings->username)
                                            ),
                                            'class' => 'follow']
                                        );
                                    endif;
                                elseif (h($followerUnfollowings->acc_status_del) == 2) :
                                        echo '<b>' . h($followerUnfollowings->full_name) . '</b><br>';
                                        echo '(deactivated)<br>';
                                        echo h($followerUnfollowings->username);
                                endif;

                                ?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </table>
                    <table style="font-size: 13px;">
                        <?php foreach ($followerFollowing as $followerFollowings) : ?>
                        <tr>
                            <td width=30%>
                                <?php if (h($followerFollowings->profile_pic) == null) :
                                    echo $this->Html->image(
                                        'default.png',
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                else :
                                    echo $this->Html->image(
                                        'profile_pic/' . h($followerFollowings->profile_pic),
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                endif; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php
                                if (h($followerFollowings->acc_status_del) == 1) :
                                    echo '<b>' . $this->Form->postLink(
                                        __(h($followerFollowings->full_name)),
                                        ['controller' => 'users', 'action' => 'timeline', h($followerFollowings->id)],
                                        ['confirm' => __(
                                            'Are you sure you want to visit {0}?',
                                            h($followerFollowings->username)
                                        ),
                                        'class' => 'side-nav-item']
                                    ) . '</b>';
                                elseif (h($followerFollowings->acc_status_del) == 2) :
                                    echo '<b>' . h($followerFollowings->full_name) . '</b><br>';
                                    echo '(deactivated)<br>';
                                endif;

                                    echo h($followerFollowings->username);
                                    echo $this->Form->postLink(
                                        __('Unfollow'),
                                        ['controller' => 'follows', 'action' => 'timelineunfollow',
                                        h($followerFollowings->id), h($profiles->id)],
                                        ['confirm' => __(
                                            'Are you sure you want to unfollow {0}?',
                                            h($followerFollowings->username)
                                        ),
                                        'class' => 'follow']
                                    );
                                ?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </table>
                </div>
            </div>
        </div>
    </aside>
</div>
<script>
    var loadCounter = "<?php echo $loadCounter; ?>";
    if(loadCounter <= 6) {
        var x = document.getElementById("loadmoreTimeline");
        x.style.display = "none";
    }
</script>