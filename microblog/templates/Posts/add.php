<?php
    /**
     * @var \App\View\AppView $this
     * @var \App\Model\Entity\Post $post
     */
    $uid = $this->request->getAttribute('identity')->getIdentifier();
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Posts'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="posts form content">
            <?= $this->Form->create($post, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Add Post') ?></legend>
                <?php
                    echo $this->Form->control('content', ['rows' => '3']);
                    echo $this->Form->control('image', ['type' => 'file']);
                    echo $this->Form->control('user_id', ['type' => 'hidden', 'value' => $uid]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>