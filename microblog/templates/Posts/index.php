<?php
    /**
     * @var \App\View\AppView $this
     * @var \App\Model\Entity\Post[]|\Cake\Collection\CollectionInterface $posts
     */
    $uid = $this->request->getAttribute('identity')->getIdentifier();
    $loadCounter = count($merged);
    $loadCounterSearch = count($mergedUserSearch);
?>
<style type="text/css">
    .countChar {
        font-size: 75% !important;
        margin-top: -2% !important;
    }
</style>
<div class="row">
    <aside class="column">
        <div class="side-nav content" style="height: 155px; position:relative;">
            <table style="font-size: 13px;">
                <?php foreach ($profile as $profiles) : ?>
                <tr>
                    <td width=30%>
                        <?php if (h($profiles->profile_pic) == null) :
                            echo $this->Html->image(
                                'default.png',
                                ['alt' => 'CakePHP', 'border' => '0', 'height' => '50px', 'width' => '50px']
                            );
                        else :
                            echo $this->Html->image(
                                'profile_pic/' . h($profiles->profile_pic),
                                ['alt' => 'CakePHP', 'border' => '0', 'height' => '50px', 'width' => '50px']
                            );
                        endif; ?>
                    </td>
                    <td>
                        <?php
                            echo '<b>' . $this->Form->postLink(
                                __(h($profiles->full_name)),
                                ['controller' => 'users', 'action' => 'timeline', h($profiles->id)]
                            ) . '</b><br>';
                            echo h($profiles->username) . '<br>';
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <center>
                <?= $this->Html->link(
                    __('Edit Profile'),
                    ['controller' => 'users', 'action' => 'profile',
                    h($profiles->id)],
                    ['class' => 'editProfile']
                ); ?>
            </center>
        </div>
    </aside>
    <div class="column-responsive column-60">
        <div class="posts form content">
            <?= $this->Form->create($post); ?>
            <fieldset>
                <?php
                    echo $this->Form->control(
                        'search',
                        ['label' => false, 'placeholder' => 'Looking for?', 'required' => true]
                    );
                    echo $this->Form->hidden('formsent', ['value' => 'searchbar']);
                    ?>
            </fieldset>
            <?= $this->Form->button(__('Search')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <aside class="column">
        <div class="side-nav content" style="height: 155px; position:relative;">
        </div>
    </aside>
</div>
<br>
<div class="row">
    <aside class="column">
        <div class="side-nav content" style="height: 1025px; position:relative;">
            <div style="max-height:100%; overflow:auto;">
            <h4 class="heading"><?= __('Who to follow') ?></h4>
            <table style="font-size: 13px;">
                <?php foreach ($toFollow as $toFollows) : ?>
                <tr>
                    <td width=30%>
                        <?php if (h($toFollows->profile_pic) == null) {
                            echo $this->Html->image(
                                'default.png',
                                ['alt' => 'CakePHP', 'border' => '0', 'height' => '50px', 'width' => '50px']
                            );
                        } else {
                            echo $this->Html->image(
                                'profile_pic/' . h($toFollows->profile_pic),
                                ['alt' => 'CakePHP', 'border' => '0',
                                'height' => '50px',
                                'width' => '50px']
                            );
                        } ?>
                    </td>
                    <td style="text-align: center;">
                        <?php
                            echo '<b>' . $this->Form->postLink(
                                __(h($toFollows->full_name)),
                                ['controller' => 'users', 'action' => 'timeline', h($toFollows->id)],
                                ['confirm' => __(
                                    'Are you sure you want to visit {0}?',
                                    h($toFollows->username)
                                ),
                                'class' => 'side-nav-item']
                            ) . '</b>';
                            echo h($toFollows->username);
                            echo $this->Form->postLink(
                                __('Follow'),
                                ['controller' => 'follows', 'action' => 'follow',
                                h($toFollows->id)],
                                ['confirm' => __(
                                    'Are you sure you want to follow {0}?',
                                    h($toFollows->username)
                                ), 'class' => 'follow']
                            );
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
        </div>
    </aside>
    <div class="column-responsive column-60">
    <?php if ($divCounter != 'search') : ?>
        <div class="posts form content" style="height: 1025px; position:relative;">
            <?= $this->Form->create($post, ['enctype' => 'multipart/form-data']); ?>
            <fieldset>
                <?php
                    echo $this->Form->control(
                        'content',
                        ['rows' => '5', 'label' => false,
                        'placeholder' => "What's on your mind?", 'id' => 'textarea']
                    );
                    echo $this->Form->label('content', '140/140', ['id' => 'count',
                        'align' => 'right', 'class' => 'countChar']);
                    echo $this->Form->control('image', ['type' => 'file', 'label' => false]);
                    echo $this->Form->control('user_id', ['type' => 'hidden', 'value' => $uid]);
                    echo $this->Form->hidden('formsent', ['value' => 'postbar']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Post')) ?>
            <?= $this->Form->end() ?>
            <?php if ($loadCounter != 0) : ?>
                <br>
                <div style="height: 73%; position:relative;">
                    <div style="max-height:100%; overflow:auto;">
                        <div  class="list">
                            <?php foreach ($merged as $all) : ?>
                                <div class="list-element">
                                    <table>
                                        <tr>
                                            <td width=20%>
                                                <?php if (h($all['image']) != null) :
                                                    echo $this->Html->image(
                                                        'post_image/' . h($all['image']),
                                                        ['alt' => 'CakePHP', 'border' => '0',
                                                        'height' => '140px', 'width' => '140px']
                                                    );
                                                else :
                                                    echo $this->Html->image(
                                                        'noimage.jpg',
                                                        ['alt' => 'CakePHP', 'border' => '0',
                                                        'height' => '140px', 'width' => '140px']
                                                    );
                                                endif; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($all['classification'] == 'post') :
                                                    if ($all['user_id'] == $uid) :
                                                        echo '<b>You wrote this post.</b><br>';
                                                    else :
                                                            echo '<b>Posted by: ' . h($all['full_name']) . '</b><br>';
                                                    endif;
                                                        echo h($all['content']) . '<br>';
                                                        echo h($all['modified']);
                                                        echo '<br>'.$this->Html->link(
                                                            $this->Html->tag('i', '', ['class' => 'fa fa-arrow-circle-right',
                                                            'style' => 'font-size: 150%;']) . ' ',
                                                            ['action' => 'view', h($all['post_id'])],
                                                            ['escape' => false]
                                                        );
                                                else :
                                                    if (h($all['repost_user_id']) == $uid) :
                                                        echo '<b>You shared this post.</b><br>';
                                                    else :
                                                            echo '<b>Shared by: ' . h($all['full_name']) . '</b><br>';
                                                    endif;
                                                        echo h($all['content']) . '<br>';
                                                        echo h($all['modified']).'<br>';
                                                    if (h($all['repost_user_id']) == $uid) :
                                                        echo $this->Form->postLink(
                                                            $this->Html->tag('i', '', ['class' => 'fa fa-trash', 
                                                                'style' => 'font-size: 150%;']).' ',
                                                            ['controller' => 'reposts', 'action' => 'delete',
                                                            h($all['repost_id'])],
                                                            ['escape' => false,
                                                            'confirm' => __('Are you sure you want to delete this?')]
                                                        );
                                                    endif;
                                                        echo $this->Html->link(
                                                            $this->Html->tag('i', '', ['class' => 'fa fa-arrow-circle-right',
                                                            'style' => 'font-size: 150%;']) . ' ',
                                                            ['action' => 'view', h($all['post_id'])],
                                                            ['escape' => false]
                                                        );
                                                endif;
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php if ($loadCounter > 4) : ?>
                            <button onclick='loadmore()' id="loadmore">View older posts...</button>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    <?php else : ?>
        <div class="posts form content" style="height: 1025px; position:relative;">
            <?php if ($countSearchPostResult == 0 && $loadCounterSearch == 0) : ?>
                <h4 class="heading"><?= __('Nothing matched the keyword you have entered...') ?></h4>
                <?php
            else :
                if ($loadCounterSearch != 0) : ?>
            <div style="height: 42%; position:relative;">
                <h4 class="heading"><?= __('User Search Results...') ?></h4>
                <div style="max-height:100%; overflow:auto;">
                    <div  class="list-UserSearch">
                        <div class="grid-container">
                            <?php foreach ($mergedUserSearch as $all) : ?>
                                <div class="list-UserSearch-element">
                                    <center>
                                        <table style="font-size: 13px; width: 80%">
                                            <tr>
                                                <td width=30%>
                                                    <?php if (h($all['profile_pic']) == null) :
                                                        echo $this->Html->image(
                                                            'default.png',
                                                            ['alt' => 'CakePHP', 'border' => '0',
                                                            'height' => '50px',
                                                            'width' => '50px']
                                                        );
                                                    else :
                                                        echo $this->Html->image(
                                                            'profile_pic/' . h($all['profile_pic']),
                                                            ['alt' => 'CakePHP', 'border' => '0',
                                                            'height' => '50px',
                                                            'width' => '50px']
                                                        );
                                                    endif; ?>
                                                </td>
                                                <td style="text-align: center;">
                                                    <?php
                                                        echo '<b>' . $this->Form->postLink(
                                                            __(h($all['full_name'])),
                                                            ['controller' => 'users', 'action' => 'timeline',
                                                            h($all['user_id'])],
                                                            ['confirm' => __(
                                                                'Are you sure you want to visit {0}?',
                                                                h($all['username'])
                                                            ),
                                                            'class' => 'side-nav-item']
                                                        ) . '</b>';
                                                        echo h($all['username']);
                                                    if ($all['status'] == 'no') :
                                                        echo $this->Form->postLink(
                                                            __('Follow'),
                                                            ['controller' => 'follows', 'action' => 'follow',
                                                            h($all['user_id'])],
                                                            ['confirm' => __(
                                                                'Are you sure you want to follow {0}?',
                                                                h($all['username'])
                                                            ),
                                                            'class' => 'follow']
                                                        );
                                                    else :
                                                            echo $this->Form->postLink(
                                                                __('Unfollow'),
                                                                ['controller' => 'follows', 'action' => 'unfollow',
                                                                h($all['user_id'])],
                                                                ['confirm' => __(
                                                                    'Are you sure you want to unfollow {0}?',
                                                                    h($all['username'])
                                                                ),
                                                                'class' => 'follow']
                                                            );
                                                    endif;
                                                    ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </center>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php if ($loadCounterSearch > 6) : ?>
                            <button onclick='loadmoreUserSearch()' id="loadmoreUserSearch">Load more...</button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <br><br>
            <br>
                <?php endif;
                if ($countSearchPostResult != 0) : ?>
            <div style="height: 42%; position:relative;">
                <h4 class="heading"><?= __('Post Search Results...') ?></h4>
                <div style="max-height:100%; overflow:auto;">
                     <div  class="list-PostSearch">
                        <?php foreach ($searchPostResult as $searchPostResults) : ?>
                            <div class="list-PostSearch-element">
                                <table>
                                    <tr>
                                        <td width=20%>
                                            <?php if (h($searchPostResults->image) != null) :
                                                echo $this->Html->image(
                                                    'post_image/' . h($searchPostResults->image),
                                                    ['alt' => 'CakePHP', 'border' => '0',
                                                    'height' => '150px', 'width' => '150px']
                                                );
                                            else :
                                                echo $this->Html->image(
                                                    'noimage.jpg',
                                                    ['alt' => 'CakePHP', 'border' => '0',
                                                    'height' => '150px', 'width' => '150px']
                                                );
                                            endif; ?>
                                        </td>
                                        <td>
                                            <?php echo h($searchPostResults->content) . '<br>';
                                                echo date(
                                                    'Y/m/d h:i:s',
                                                    strtotime(h($searchPostResults->modified) . ' +8 hours')
                                                );
                                                echo '<br>'.$this->Html->link(
                                                    $this->Html->tag('i', '', ['class' => 'fa fa-arrow-circle-right',
                                                    'style' => 'font-size: 150%;']) . ' ',
                                                    ['action' => 'view', h($searchPostResults->id)],
                                                    ['escape' => false]
                                                );
                                            ?>
                                        </td>
                                    </tr> 
                                </table>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php if ($countSearchPostResult > 3) : ?>
                        <button onclick='loadmorePostSearch()' id="loadmorePostSearch">Load More...</button>
                    <?php endif; ?>
                </div>
            </div>
                <?php endif;
            endif; ?>
        </div>
    <?php endif; ?>
    </div>
    <aside class="column">
        <div class="side-nav content" style="height: 1025px; position:relative;">
            <div style="height: 42%; position:relative;">
                <h4 class="heading"><?= __('Your followed users') ?></h4>
                <div style="max-height:100%; overflow:auto;">
                    <table style="font-size: 13px;">
                        <?php foreach ($follows as $followings) : ?>
                        <tr>
                            <td width=30%>
                                <?php if (h($followings->profile_pic) == null) :
                                    echo $this->Html->image(
                                        'default.png',
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                else :
                                    echo $this->Html->image(
                                        'profile_pic/' . h($followings->profile_pic),
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                endif; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php
                                if (h($followings->acc_status_del) == 1) :
                                    echo '<b>' . $this->Form->postLink(
                                        __(h($followings->full_name)),
                                        ['controller' => 'users', 'action' => 'timeline', h($followings->id)],
                                        ['confirm' => __(
                                            'Are you sure you want to visit {0}?',
                                            h($followings->username)
                                        ),
                                        'class' => 'side-nav-item']
                                    ) . '</b>';
                                elseif (h($followings->acc_status_del) == 2) :
                                        echo '<b>' . h($followings->full_name) . '</b><br>';
                                        echo '(deactivated)<br>';
                                endif;

                                    echo h($followings->username);
                                    echo $this->Form->postLink(
                                        __('Unfollow'),
                                        ['controller' => 'follows', 'action' => 'unfollow', h($followings->id)],
                                        ['confirm' => __(
                                            'Are you sure you want to unfollow {0}?',
                                            h($followings->username)
                                        ),
                                        'class' => 'follow']
                                    ); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <br><br>
            <br>
            <div style="height: 42%; position:relative;">
                <h4 class="heading"><?= __('Your followers') ?></h4>
                <div style="max-height:100%; overflow:auto;">
                    <table style="font-size: 13px;">
                    <?php foreach ($followerUnfollowing as $followerUnfollowings) : ?>
                    <tr>
                        <td width=30%>
                            <?php if (h($followerUnfollowings->profile_pic) == null) :
                                echo $this->Html->image(
                                    'default.png',
                                    ['alt' => 'CakePHP', 'border' => '0',
                                    'height' => '50px',
                                    'width' => '50px']
                                );
                            else :
                                echo $this->Html->image(
                                    'profile_pic/' . h($followerUnfollowings->profile_pic),
                                    ['alt' => 'CakePHP', 'border' => '0',
                                    'height' => '50px',
                                    'width' => '50px']
                                );
                            endif; ?>
                        </td>
                        <td style="text-align: center;">
                            <?php
                            if (h($followerUnfollowings->acc_status_del) == 1) :
                                echo '<b>' . $this->Form->postLink(
                                    __(h($followerUnfollowings->full_name)),
                                    ['controller' => 'users', 'action' => 'timeline', h($followerUnfollowings->id)],
                                    ['confirm' => __(
                                        'Are you sure you want to visit {0}?',
                                        h($followerUnfollowings->username)
                                    ),
                                    'class' => 'side-nav-item']
                                ) . '</b>';
                                echo h($followerUnfollowings->username);
                                echo $this->Form->postLink(
                                    __('Follow'),
                                    ['controller' => 'follows', 'action' => 'follow', h($followerUnfollowings->id)],
                                    ['confirm' => __(
                                        'Are you sure you want to follow {0}?',
                                        h($followerUnfollowings->username)
                                    ),
                                    'class' => 'follow']
                                );
                            elseif (h($followerUnfollowings->acc_status_del) == 2) :
                                    echo '<b>' . h($followerUnfollowings->full_name) . '</b><br>';
                                    echo '(deactivated)<br>';
                                    echo h($followerUnfollowings->username);
                            endif;

                            ?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    </table>
                    <table style="font-size: 13px;">
                        <?php foreach ($followerFollowing as $followerFollowings) : ?>
                        <tr>
                            <td width=30%>
                                <?php if (h($followerFollowings->profile_pic) == null) :
                                    echo $this->Html->image(
                                        'default.png',
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                else :
                                    echo $this->Html->image(
                                        'profile_pic/' . h($followerFollowings->profile_pic),
                                        ['alt' => 'CakePHP', 'border' => '0',
                                        'height' => '50px',
                                        'width' => '50px']
                                    );
                                endif; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php
                                if (h($followerFollowings->acc_status_del) == 1) :
                                    echo '<b>' . $this->Form->postLink(
                                        __(h($followerFollowings->full_name)),
                                        ['controller' => 'users', 'action' => 'timeline', h($followerFollowings->id)],
                                        ['confirm' => __(
                                            'Are you sure you want to visit {0}?',
                                            h($followerFollowings->username)
                                        ),
                                        'class' => 'side-nav-item']
                                    ) . '</b>';
                                elseif (h($followerFollowings->acc_status_del) == 2) :
                                        echo '<b>' . h($followerFollowings->full_name) . '</b><br>';
                                        echo '(deactivated)<br>';
                                endif;

                                    echo h($followerFollowings->username);
                                    echo $this->Form->postLink(
                                        __('Unfollow'),
                                        ['controller' => 'follows', 'action' => 'unfollow', h($followerFollowings->id)],
                                        ['confirm' => __(
                                            'Are you sure you want to unfollow {0}?',
                                            h($followerFollowings->username)
                                        ),
                                        'class' => 'follow']
                                    );
                                ?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </table>
                </div>
            </div>
        </div>
    </aside>
</div>

<script type="text/javascript">
    document.getElementById('textarea').onkeyup = function () {
        document.getElementById('count').innerHTML = (140 - this.value.length) + '/140';
    };
</script>

                   
