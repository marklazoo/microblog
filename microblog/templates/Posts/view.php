<?php
    /**
     * @var \App\View\AppView $this
     * @var \App\Model\Entity\Post $post
     */
    $uid = $this->request->getAttribute('identity')->getIdentifier();
?>
<style type="text/css">
    .countChar {
        font-size: 75% !important;
        margin-top: -2% !important;
    }
</style>
<div class="viewPostContent">
    <div class="column-responsive column-80" style="margin-right: 10px">
        <div class="posts view" style="height: 1525px; position:relative;"  id='viewPost'>
            <table>
                <?php foreach ($posterInfo as $posterInfos) : ?>
                <tr>
                    <td width=8%>
                        <center>
                            <?php if (h($posterInfos->profile_pic) == null) :
                                echo $this->Html->image(
                                    'default.png',
                                    ['alt' => 'CakePHP', 'border' => '0',
                                    'height' => '50px',
                                    'width' => '50px']
                                );
                            else :
                                echo $this->Html->image(
                                    'profile_pic/' . h($posterInfos->profile_pic),
                                    ['alt' => 'CakePHP', 'border' => '0',
                                    'height' => '50px',
                                    'width' => '50px']
                                );
                            endif; ?>
                        </center>
                    </td>
                    <td width=60%>
                        <?php echo "<font style='font-size: 20px;'>" . h($posterInfos->full_name) . '</font><br>';
                            echo "<font style='font-size: 15px;'>" . h($posterInfos->username) . '</font>';?>
                    </td>
                    <td style="text-align: right;">
                        <?php echo date('Y/m/d h:i:s', strtotime(h($post->created) . ' +8 hours')); ?><br>
                        <?php if ($post->user->id == $uid) :
                            echo $this->Html->link(
                                $this->Html->tag('i', '', ['class' => 'fa fa-edit',
                                'style' => 'font-size: 200%;']) . ' ',
                                ['action' => 'edit', $post->id],
                                ['escape' => false]
                            );
                            echo $this->Form->postLink(
                                $this->Html->tag('i', '', ['class' => 'fa fa-trash', 'style' => 'font-size: 200%;']),
                                ['action' => 'delete', $post->id],
                                ['escape' => false,
                                'confirm' => __('Are you sure you want to delete # {0}?', $post->id)]
                            );
                        endif; ?>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
            <br>
            <table>
                <tr>
                    <td><?= h($post->content) ?></td>
                </tr>
                <tr>
                    <td>
                        <center>
                            <?php if (h($post->image) != null) :
                                echo $this->Html->image(
                                    'post_image/' . h($post->image),
                                    ['alt' => 'CakePHP', 'border' => '0',
                                    'height' => '350px']
                                );
                            else :
                                echo $this->Html->image(
                                    'noimage.jpg',
                                    ['alt' => 'CakePHP', 'border' => '0',
                                    'height' => '350px']
                                );
                            endif; ?>
                        </center> 
                    </td>
                </tr>
            </table>

            <?php
            if ($countTotal != 0) :
                echo $countTotal . ' like(s) <br>';
            else :
                    echo 'Be the first one to like this post.<br>';
            endif;

            if ($count != 0) :
                echo $this->Form->postLink(
                    $this->Html->tag('i', '', ['class' => 'fa fa-thumbs-up',
                        'style' => 'font-size: 200%;']),
                    ['controller' => 'likes', 'action' => 'unlike',
                    h($post->id)],
                    ['escape' => false]
                );
            else :
                echo $this->Form->postLink(
                    $this->Html->tag('i', '', ['class' => 'fa fa-thumbs-o-up',
                        'style' => 'font-size: 200%;']),
                    ['controller' => 'likes', 'action' => 'like',
                    h($post->id)],
                    ['escape' => false]
                );
            endif;
                echo $this->Form->postLink(
                    $this->Html->tag('i', '', ['class' => 'fa fa-share-square-o',
                        'style' => 'font-size: 200%;']),
                    ['controller' => 'reposts', 'action' => 'share',
                    h($post->id)],
                    ['escape' => false]
                );
                ?>
            <br><br>
            <div id='addComment'>
                <?= $this->Form->create(null, ['url' => ['controller' => 'comments', 'action' => 'add']]) ?>
                <fieldset>
                    <?php
                        echo $this->Form->control('post_id', ['type' => 'hidden', 'value' => $post->id]);
                        echo $this->Form->control(
                            'comment_text',
                            ['rows' => '5', 'label' => false,
                            'placeholder' => 'Type your comments here...',
                            'required' => true,
                            'maxlength' => '140', 'id' => 'textarea']
                        );
                        echo $this->Form->label('comment_text', '140/140', ['id' => 'count',
                            'align' => 'right', 'class' => 'countChar']);
                        ?>
                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
            <div id='editComment' style="display: none">
                <?= $this->Form->create(null, ['url' => ['controller' => 'comments', 'action' => 'edit']]) ?>
                <fieldset>
                    <?php
                        echo $this->Form->control('post_id', ['type' => 'hidden', 'value' => $post->id]);
                        echo $this->Form->control('comment_id', ['type' => 'hidden', 'id' => 'comment_id']);
                        echo $this->Form->control(
                            'comment_text',
                            ['rows' => '5', 'label' => false,
                            'placeholder' => 'Type your comments here...',
                            'required' => true, 'id' => 'comment_text_edit',
                            'maxlength' => '140']
                        );
                        echo $this->Form->label('comment_text', '140/140', ['id' => 'countEdit',
                            'align' => 'right', 'class' => 'countChar']);
                        ?>
                </fieldset>
                <?= $this->Form->button(__('Save Changes')) ?>
                <?= $this->Form->end() ?>
                <button onclick='cancel();' style="display: inline-block;">Disregard Changes</button>
            </div>
            <?php if ($countComments != 0) : ?>
            <div style="height: 16%; position:relative;" id='comments'>
                <h4><?= __('Related Comments') ?></h4>
                <div style="max-height:100%; overflow:auto;">
                    <div  class="list-comment">
                        <?php foreach ($commentInfo as $commentInfos) :
                            $comment_text = h($commentInfos['c']['comment_text']);
                            $comment_id = h($commentInfos['c']['id']); ?>
                            <div class="list-comment-element">
                                <table>
                                    <tr>
                                        <td width=8%>
                                            <?php if (h($commentInfos->profile_pic) == null) :
                                                echo $this->Html->image(
                                                    'default.png',
                                                    ['alt' => 'CakePHP', 'border' => '0',
                                                    'height' => '50px',
                                                    'width' => '50px']
                                                );
                                            else :
                                                echo $this->Html->image(
                                                    'profile_pic/' . h($commentInfos->profile_pic),
                                                    ['alt' => 'CakePHP', 'border' => '0',
                                                    'height' => '50px',
                                                    'width' => '50px']
                                                );
                                            endif; ?>
                                        </td>
                                        <td width=15%>
                                            <?php echo h($commentInfos->full_name) . '<br>';
                                                echo h($commentInfos->username); ?>
                                        </td>
                                        <td width=55%>
                                            <?= h($commentInfos['c']['comment_text']) . '<br>' ?>
                                        </td>
                                        <td style="text-align: right;">
                                            <?= date(
                                                    'Y/m/d H:i:s',
                                                    strtotime(h($commentInfos['c']['modified']) . ' +8 hours')
                                                ) . '<br>' ?>
                                            <?php if (h($commentInfos->id) == $uid) : ?>
                                            <i class='fa fa-edit' style = 'font-size: 150%;' 
                                            onclick='editComment(<?php echo '"' .
                                            $comment_text . '", ' . $post->id . ', ' .
                                            $comment_id;?>);'></i>
                                            <?php
                                                echo $this->Form->postLink(
                                                    $this->Html->tag('i', '', ['class' => 'fa fa-trash',
                                                        'style' => 'font-size: 150%;']),
                                                    ['controller' => 'Comments', 'action' => 'delete',
                                                    h($commentInfos['c']['id'])],
                                                    ['escape' => false,
                                                    'confirm' => __(
                                                        'Are you sure you want to delete # {0}?',
                                                        h($commentInfos['c']['id'])
                                                    )]
                                                );
                                            endif; ?>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <button onclick='loadmoreComment()' id="loadmoreComment">View older comments...</button>
                </div>
            </div>
            <br><br>
            <br>
            <?php endif;
            if ($post->user->id == $uid) :
                if ($countTotal != 0) :
                    echo "<button onclick='likes()' id='showLikes'><i class='fa fa-navicon'></i> Likes</button>";
                endif;

                if ($countShares != 0) :
                    echo "<button onclick='shares()' id='showShares'><i class='fa fa-navicon'></i> Shares</button>";
                endif;
                if ($countShares != 0) : ?>
                <div style="height: 16%; position:relative; display: none;" id='shares'>
                    <h4><?= __('People who shared your post.') ?></h4>
                    <div style="max-height:100%; overflow:auto;">
                        <?php foreach ($repostInfo as $repostInfos) : ?>
                            <table>
                                <tr>
                                    <td width=8%>
                                        <?php if (h($repostInfos->profile_pic) == null) :
                                            echo $this->Html->image(
                                                'default.png',
                                                ['alt' => 'CakePHP', 'border' => '0',
                                                'height' => '50px',
                                                'width' => '50px']
                                            );
                                        else :
                                            echo $this->Html->image(
                                                'profile_pic/' . h($repostInfos->profile_pic),
                                                ['alt' => 'CakePHP', 'border' => '0',
                                                'height' => '50px',
                                                'width' => '50px']
                                            );
                                        endif; ?>
                                    </td>
                                    <td width=15%>
                                        <?php echo h($repostInfos->full_name) . '<br>';
                                            echo h($repostInfos->username); ?>
                                    </td>
                                    <td>
                                        <?php echo 'Shared on: ' . date(
                                            'Y/m/d h:i:s',
                                            strtotime(h($repostInfos['c']['created']) . ' +8 hours')
                                        ); ?>
                                    </td>
                                </tr>
                            </table>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endif;
                if ($countTotal != 0) : ?>
                <div style="height: 16%; position:relative; display: none;" id='likes'>
                    <h4><?= __('People who liked your post.') ?></h4>
                    <div style="max-height:100%; overflow:auto;">
                        <?php foreach ($listLiker as $listLikers) : ?>
                            <table>
                                <tr>
                                    <td width=8%>
                                        <?php if (h($listLikers->profile_pic) == null) :
                                            echo $this->Html->image(
                                                'default.png',
                                                ['alt' => 'CakePHP', 'border' => '0',
                                                'height' => '50px',
                                                'width' => '50px']
                                            );
                                        else :
                                            echo $this->Html->image(
                                                'profile_pic/' . h($listLikers->profile_pic),
                                                ['alt' => 'CakePHP', 'border' => '0',
                                                'height' => '50px',
                                                'width' => '50px']
                                            );
                                        endif; ?>
                                    </td>
                                    <td width=15%>
                                        <?php echo h($listLikers->full_name) . '<br>';
                                            echo h($listLikers->username); ?>
                                    </td>
                                    <td>
                                        <?php echo 'Liked on: ' . date(
                                            'Y/m/d H:i:s',
                                            strtotime(h($listLikers['c']['created']) . ' +8 hours')
                                        ); ?>
                                    </td>
                                </tr>
                            </table>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endif;
            endif; ?>
        </div>
    </div>
</div>

<script>
    var uid = "<?php echo $uid; ?>";
    var user_id = "<?php echo $post->user->id; ?>";
    var countComments = "<?php echo $countComments; ?>";

    if(countComments <= 3) {
        document.getElementById("loadmoreComment").style.display = "none";
    }

    if(user_id != uid) {
        document.getElementById("viewPost").style.height = "1200px";
        document.getElementById("comments").style.height = "25%";
    }

    document.getElementById('textarea').onkeyup = function () {
        document.getElementById('count').innerHTML = (140 - this.value.length) + '/140';
    };

    document.getElementById('comment_text_edit').onkeyup = function () {
        document.getElementById('countEdit').innerHTML = (140 - this.value.length) + '/140';
    };
</script>


                    