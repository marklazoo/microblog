<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 */
?>

<div class="editProfileContent">
    <div class="users form">
        <table>
            <tr>
                <td style="text-align: right;">
                    <?= $this->Form->postLink(
                                $this->Html->tag('i', '', ['class' => 'fa fa-trash', 'style' => 'font-size: 200%;']),
                                ['action' => 'delete', $post->id],
                                ['escape' => false,
                                'confirm' => __('Are you sure you want to delete # {0}?', $post->id)]
                            ) ?>
                </td>
            </tr>
        </table>
        <?= $this->Form->create($post, ['enctype' => 'multipart/form-data']) ?>
        <fieldset>
            <?php
            if (h($post->image) != null) :
                echo '<center>' . $this->Html->image(
                    'post_image/' . h($post->image),
                    ['alt' => 'CakePHP', 'border' => '0', 'height' => '350px']
                ) . '</center>';
            endif;

                    //echo $this->Form->control('user_id', ['options' => $users]);
                echo $this->Form->control('content', ['rows' => '3', 'label' => false]);
                echo $this->Form->control('image', ['type' => 'file', 'label' => false]);
                    //echo $this->Form->control('post_status_del');
                    //echo $this->Form->control('created');
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
