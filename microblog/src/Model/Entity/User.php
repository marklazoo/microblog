<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $full_name
 * @property \Cake\I18n\FrozenDate $birthdate
 * @property string $email
 * @property string $password
 * @property string $profile_pic
 * @property string $activation_code
 * @property \Cake\I18n\FrozenTime $email_activated
 * @property int $acc_status_del
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Follow[] $follows
 * @property \App\Model\Entity\Like[] $likes
 * @property \App\Model\Entity\Post[] $posts
 * @property \App\Model\Entity\Repost[] $reposts
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'full_name' => true,
        'birthdate' => true,
        'email' => true,
        'password' => true,
        'profile_pic' => true,
        'activation_code' => true,
        'email_activated' => true,
        'acc_status_del' => true,
        'created' => true,
        'modified' => true,
        'comments' => true,
        'follows' => true,
        'likes' => true,
        'posts' => true,
        'reposts' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    protected function _setPassword(string $password) : ?string {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher())->hash($password);
        } 
    }
}
