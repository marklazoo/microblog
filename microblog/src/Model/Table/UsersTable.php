<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\FollowsTable&\Cake\ORM\Association\HasMany $Follows
 * @property \App\Model\Table\LikesTable&\Cake\ORM\Association\HasMany $Likes
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 * @property \App\Model\Table\RepostsTable&\Cake\ORM\Association\HasMany $Reposts
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Comments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Follows', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Likes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Posts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Reposts', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmptyString('username');

        $validator
            ->add('username', 'customUsername', [
                'rule' => [$this, 'alphaNumericDashUnderscore'],
                'message' => 'Alphanumeric and underscores only.'
            ]);

        $validator
            ->scalar('full_name')
            ->maxLength('full_name', 255)
            ->requirePresence('full_name', 'create')
            ->notEmptyString('full_name');

        $validator
            ->add('full_name', 'customFullname', [
                'rule' => [$this, 'alphaNumericDashPeriod'],
                'message' => 'Alphanumeric, dash and period only.'
            ]);

        $validator
            ->date('birthdate')
            ->requirePresence('birthdate', 'create')
            ->notEmptyDate('birthdate');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->minLength('password', 8, 'Password should be atleast 8 characters.')
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->add('password', 'customPass', [
                'rule' => [$this, 'passwordCheck'],
                'message' => 'Password requirements not met!'
            ]);

        $validator
            ->scalar('activation_code')
            ->maxLength('activation_code', 255)
            ->minLength('activation_code', 10, 'Check your email.')
            //->requirePresence('activation_code', 'create')
            ->allowEmptyString('activation_code');

        $validator
            ->dateTime('email_activated')
            ->allowEmptyDateTime('email_activated', null, 'create');

        $validator
            ->integer('acc_status_del')
            ->notEmptyString('acc_status_del');

        $validator
            ->scalar('newPassword')
            ->maxLength('newPassword', 255)
            ->minLength('newPassword', 8, 'Password should be atleast 8 characters.')
            ->allowEmptyString('newPassword')
            ->sameAs('newPassword', 'confirmPassword', 'New Password and Confirm Password should match.');

        $validator
            ->add('newPassword', 'customPass', [
                'rule' => [$this, 'passwordCheck'],
                'message' => 'Password requirements not met!'
            ]);

        $validator
            ->scalar('confirmPassword')
            ->maxLength('confirmPassword', 255)
            ->minLength('confirmPassword', 8, 'Password should be atleast 8 characters.')
            ->allowEmptyString('confirmPassword')
            ->sameAs('confirmPassword', 'newPassword', 'New Password and Confirm Password should match.');

        return $validator;
    }

    public function alphaNumericDashUnderscore($username) {
        $try = preg_match("/^[a-zA-Z0-9_]*$/", $username);
        if($try == 1) {
            return true;
        } else {
            return false;
        }
        // return $try;
    }

    public function alphaNumericDashPeriod($fullname) {
        $try = preg_match("/^[a-zA-Z0-9-. ]*$/", $fullname);
        if($try == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function passwordCheck($password) {
        $count = 0;
        if(preg_match("/[a-z]/", $password)) {
            $count = $count + 1;
        }
        if(preg_match("/[A-Z]/", $password)) {
            $count = $count + 1;
        }
        if(preg_match("/[0-9]/", $password)) {
            $count = $count + 1;
        }
        if($count == 3) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);

        return $rules;
    }
}
