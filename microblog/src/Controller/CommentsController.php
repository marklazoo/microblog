<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            if ($this->getRequest()->is('post')) {
                $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();
                $post_id = $this->getRequest()->getData('post_id');
                $comment_text = $this->getRequest()->getData('comment_text');

                $comment = $this->Comments->newEntity([
                    'user_id' => $uid,
                    'post_id' => $post_id,
                    'comment_text' => $comment_text,
                ]);

                if ($this->Comments->save($comment)) {
                    $this->Flash->success(__('Your commented this post.'));
                } else {
                    $this->Flash->error(__('Unable to save comment. Please, try again!'));
                }

                return $this->redirect([
                    'controller' => 'posts',
                    'action' => 'view',
                    $post_id,
                ]);
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * Edit method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $post_id = $this->getRequest()->getData('post_id');
            $comment_id = $this->getRequest()->getData('comment_id');
            $comment_text = $this->getRequest()->getData('comment_text');
            $editComment = $this->Comments->get($comment_id);

            if (!$this->isAuthor($editComment->user_id)) {
                if ($this->getRequest()->is('post')) {
                    $currDateTime = date('Y-m-d H:i:s');
                    $editComment->comment_text = $comment_text;
                    $editComment->modified = $currDateTime;

                    if ($this->Comments->save($editComment)) {
                        $this->Flash->success(__('Changes to comment is saved.'));
                    } else {
                        $this->Flash->error(__('Unable to save changes. Please, try again!'));
                    }

                    return $this->redirect([
                        'controller' => 'posts',
                        'action' => 'view',
                        $post_id,
                    ]);
                }
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $deleteComment = $this->Comments->get($id);
            if (!$this->isAuthor($deleteComment->user_id)) {
                if ($this->getRequest()->is('post')) {
                    $currDateTime = date('Y-m-d H:i:s');

                    $deleteComment->comment_status_del = 1;
                    $deleteComment->modified = $currDateTime;

                    if ($this->Comments->save($deleteComment)) {
                        $this->Flash->success(__('The comment has been deleted.'));
                    } else {
                        $this->Flash->error(__('Unable to delete comment. Please, try again!'));
                    }

                    $query = $this->Comments
                        ->find()
                        ->select(['post_id'])
                        ->where(['id' => $id]);
                    foreach ($query->all() as $result) {
                        return $this->redirect([
                            'controller' => 'posts',
                            'action' => 'view',
                            $result['post_id'],
                        ]);
                    }
                }
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }
}
