<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Follows Controller
 *
 * @property \App\Model\Table\FollowsTable $Follows
 * @method \App\Model\Entity\Follow[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowsController extends AppController
{
    /**
     * follow method
     *
     * @param string|null $id Follow id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     */
    public function follow($id = null)
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();
            if ($this->getRequest()->is('post')) {
                $follow = $this->Follows->newEmptyEntity();
                $follow->follower_user_id = $uid;
                $follow->user_id = $id;

                if ($this->Follows->save($follow)) {
                    $this->Flash->success(__('Followed successful.'));
                } else {
                    $this->Flash->error(__('Unable to follow. Please, try again!'));
                }

                return $this->redirect([
                    'controller' => 'posts',
                    'action' => 'index',
                ]);
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * unfollow method
     *
     * @param string|null $id Follow id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function unfollow($id = null)
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();
            if ($this->getRequest()->is('post')) {
                $toUnfollow = $this->Follows->query();
                $toUnfollow->delete()
                    ->where([
                        'AND' => [
                            'user_id' => $id,
                            'follower_user_id' => $uid,
                        ],
                    ])
                    ->execute();
                $this->Flash->success(__('Unfollow is successful.'));

                return $this->redirect([
                    'controller' => 'posts',
                    'action' => 'index',
                ]);
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }
}
