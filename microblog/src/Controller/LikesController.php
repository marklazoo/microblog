<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 * @method \App\Model\Entity\Like[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikesController extends AppController
{
    /**
     * like method
     *
     * @param string|null $id Like id.
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function like($id = null)
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();
            if ($this->getRequest()->is('post')) {
                $likePost = $this->Likes->newEntity([
                    'post_id' => $id,
                    'user_id' => $uid,
                ]);

                if ($this->Likes->save($likePost)) {
                    $this->Flash->success(__('You liked this post.'));
                } else {
                    $this->Flash->error(__('Unable to like this post.Please, try again!'));
                }

                return $this->redirect([
                    'controller' => 'posts',
                    'action' => 'view',
                    $id,
                ]);
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * unlike method
     *
     * @param string|null $id Like id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function unlike($id = null)
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();
            if ($this->getRequest()->is('post')) {
                $toUnlike = $this->Likes->query();
                $toUnlike->delete()
                    ->where([
                        'AND' => [
                            'user_id' => $uid,
                            'post_id' => $id,
                        ],
                    ])
                    ->execute();
                $this->Flash->error(__('You unliked this post.'));

                return $this->redirect([
                    'controller' => 'posts',
                    'action' => 'view',
                    $id,
                ]);
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }
}
