<?php
declare(strict_types=1);

namespace App\Controller;

//use Cake\Mailer\Email;
use Cake\Mailer\Mailer;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     *
     * @param \Cake\Event\EventInterface $event blog event
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('loggedout');
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['login', 'add','activate', 'forgotpassword']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        if ($this->Authentication->getIdentity()) {
            return $this->redirect(['action' => 'logout']);
        } else {
            return $this->redirect(['action' => 'login']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        $activation = $this->generateRandomString();

        if ($this->getRequest()->is('post')) {
            $user = $this->Users->patchEntity($user, $this->getRequest()->getData());

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                $mailer = new Mailer();
                $mailer ->setEmailFormat('html')
                    ->setTo($user['email'])
                    ->setFrom('microblog.cake@gmail.com')
                    ->setSubject('MicroBlog Email Activation.')
                    ->viewBuilder()
                    ->setTemplate('activation_code');

                $mailer ->setViewVars([
                    'activationCode' => $user['activation_code'],
                    'name' => $user['full_name'],
                    ])
                    ->deliver();

                return $this->redirect(['action' => 'activate']);
            }
            $this->Flash->error(__('Failed: Please, try again.'));
        }
        $this->set(compact('activation', 'user'));
    }

    /**
     * profile method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function profile($id = null)
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $user = $this->Users->get($id, [
                'contain' => [],
            ]);

            if (!$this->isAuthor($id)) {
                $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();
                if ($this->getRequest()->is('put')) {
                    $user = $this->Users->newEmptyEntity();
                    $users = $this->Users->get($id);
                    $userData = $this->getRequest()->getData();

                    $profile_pic = $this->getRequest()->getData('profile_pic');
                    $username = $this->getRequest()->getData('username');
                    $full_name = $this->getRequest()->getData('full_name');
                    $birthdate = $this->getRequest()->getData('birthdate');
                    $password = $this->getRequest()->getData('newPassword');
                    $confirmPassword = $this->getRequest()->getData('confirmPassword');

                    $name = $profile_pic->getClientFilename();
                    $type = $profile_pic->getClientMediaType();
                    if (!empty($name)) {
                        if ($type == 'image/jpeg' || $type == 'image/jpg' || $type == 'image/png') {
                            $ext = substr(strtolower(strrchr($name, '.')), 1);
                            $newName = $uid . '.' . $ext;
                            $targetPath = WWW_ROOT . 'img' . DS . 'profile_pic' . DS . $newName;
                            if ($profile_pic->getSize() > 0 && $profile_pic->getError() == 0) {
                                $profile_pic->moveTo($targetPath);
                                $userData['profile_pic'] = $newName;
                            }
                        } else {
                            $this->Flash->error(__('Failed Save: Incorrect image extension. Use jpeg, jpg, & png.'));

                            return $this->redirect([
                                'action' => 'profile',
                                $id,
                            ]);
                        }
                    } else {
                        $userData['profile_pic'] = $users->profile_pic;
                    }

                    $checkChanges = $this->Users
                        ->find()
                        ->select(['username'])
                        ->where([
                            'AND' => [
                                'username' => $username,
                                'full_name' => $full_name,
                                'birthdate' => $birthdate,
                            ],
                        ]);
                    $changesCounter = $checkChanges->count();

                    if ($changesCounter != 0) {
                        if ($password == '' && $confirmPassword == '' && empty($name)) {
                            $this->Flash->error(__('No changes to be saved.'));

                            return $this->redirect([
                                'action' => 'profile',
                                $uid,
                            ]);
                        }
                    }

                    if ($password != '') {
                        $userData['password'] = $password;
                    }

                    $user = $this->Users->patchEntity($users, $userData);
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('Profile detail changes are applied.'));

                        return $this->redirect([
                            'action' => 'profile',
                            $uid,
                        ]);
                    }

                    $this->Flash->error(__('Failed Save: Please try again.'));
                }

                $this->set(compact('user'));
            }
        } else {
            return $this->redirect(['action' => 'login']);
        }
    }

    /**
     * deactivate method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deactivate($id = null)
    {
        $newpass = $this->generateRandomString();

        if ($this->Authentication->getIdentity()) {
            if (!$this->isAuthor($id)) {
                if ($this->getRequest()->is('post')) {
                    $currDateTime = date('Y-m-d H:i:s');

                    $deactivate = $this->Users->get($id);
                    $deactivate->acc_status_del = 2;
                    $deactivate->password = $newpass;
                    $deactivate->modified = $currDateTime;

                    if ($this->Users->save($deactivate)) {
                        $currDateTime = date('Y/m/d h:i:s', strtotime(date('Y-m-d H:i:s') . ' +8 hours'));
                        $query = $this->Users
                            ->find()
                            ->select()
                            ->where(['id' => $id]);
                        foreach ($query->all() as $result) {
                            $mailer = new Mailer();
                            $mailer ->setEmailFormat('html')
                                ->setTo($result['email'])
                                ->setFrom('microblog.cake@gmail.com')
                                ->setSubject('Account Deactivated.')
                                ->viewBuilder()
                                ->setTemplate('deactivate');

                            $mailer ->setViewVars(['time' => $currDateTime, 'name' => $result['full_name']])
                                ->deliver();
                        }
                        $this->Flash->error(__('Account has been deactivated.'));

                        return $this->redirect(['action' => 'logout']);
                    }
                    $this->Flash->error(__('Deactivate Failed. Please, try again!'));
                }
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * login method
     *
     * @return \Cake\Http\Response|null|void Redirects to login.
     */
    public function login()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $user = $this->Users->patchEntity($user, $this->getRequest()->getData());
            $this->getRequest()->allowMethod('post');
            $query = $this->Users
                ->find()
                ->select(['acc_status_del'])
                ->where(['email' => $user['email']]);
            $number = $query->count();

            if ($number != 0) {
                foreach ($query->all() as $result) {
                    if ($result['acc_status_del'] == 0) {
                        $this->Flash->error(__('Failed: Email not verified.'));
                    } elseif ($result['acc_status_del'] == 1) {
                        $result = $this->Authentication->getResult();
                        if ($result->isValid()) {
                            $redirect = $this->getRequest()->getQuery('redirect', [
                                'controller' => 'Posts',
                                'action' => 'index',
                            ]);

                            return $this->redirect($redirect);
                        }
                        if ($this->getRequest()->is('post') && !$result->isValid()) {
                            $this->Flash->error(__('Failed: Invalid email or password'));
                        }
                    } elseif ($result['acc_status_del'] == 2) {
                        $this->Flash->error(__('Failed: Email not activated.'));
                    }
                }
            } else {
                $this->Flash->error(__('Failed: Email does not exist.'));
            }
        }
    }

    /**
     * logout method
     *
     * @return \Cake\Http\Response|null|void Redirects to login.
     */
    public function logout()
    {
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $this->Authentication->logout();

            return $this->redirect(['action' => 'login']);
        }
    }

    /**
     * generateRandomString method
     *
     * @param int|10 $length .
     * @return \Cake\Http\Response|$randomString.
     */
    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * activate method
     *
     * @return \Cake\Http\Response|null|void Redirects to login.
     */
    public function activate()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $user = $this->Users->patchEntity($user, $this->getRequest()->getData());
            $email = $this->getRequest()->getData('email');

            $query = $this->Users
                ->find()
                ->select(['email'])
                ->where(['email' => $email]);
            $number = $query->count();

            $newpass = $this->generateRandomString();

            if ($number != 0) {
                if ($this->getRequest()->getData('activate') != null) {
                    if ($user['activation_code'] != null) {
                        $query = $this->Users
                            ->find()
                            ->select(['activation_code', 'acc_status_del', 'full_name', 'id'])
                            ->where(['email' => $user['email']]);
                        foreach ($query->all() as $result) {
                            $currentStatus = $result['acc_status_del'];
                            if ($currentStatus == 1) {
                                $this->Flash->success(__('Email is already verified/activated.'));
                            } else {
                                $currDateTime = date('Y-m-d H:i:s');
                                if ($result['activation_code'] == $user['activation_code']) {
                                    $accountActivate = $this->Users->get($result['id']);
                                    $accountActivate->acc_status_del = 1;
                                    $accountActivate->email_activated = $currDateTime;

                                    if ($this->Users->save($accountActivate)) {
                                        if ($currentStatus == 0) {
                                            $this->Flash->success(__('Email verified.'));
                                        } elseif ($currentStatus == 2) {
                                            $temp = $this->Users->get($result['id']);
                                            $temp->password = $newpass;
                                            if ($this->Users->save($temp)) {
                                                $mailer = new Mailer();
                                                $mailer ->setEmailFormat('html')
                                                    ->setTo($user['email'])
                                                    ->setFrom('microblog.cake@gmail.com')
                                                    ->setSubject('MicroBlog Temporary Password.')
                                                    ->viewBuilder()
                                                    ->setTemplate('default');

                                                $mailer ->setViewVars([
                                                    'newPass' => $newpass,
                                                    'name' => $result['full_name'],
                                                ])
                                                    ->deliver();

                                                $this->Flash->success(
                                                    __('Account re-activated. Check email for temporary password.')
                                                );
                                            } else {
                                                $this->Flash->error(__('Failed: Please, try again!'));
                                            }
                                        }
                                    }

                                    return $this->redirect(['action' => 'login']);
                                } else {
                                    $this->Flash->error(__('Failed: Incorrect activation code.'));
                                }
                            }
                        }
                    } else {
                        $this->Flash->error(__('Failed: Enter code to activate.'));
                    }
                } elseif ($this->getRequest()->getData('resend') != null) {
                    $newCode = $this->generateRandomString();
                    $query = $this->Users
                        ->find()
                        ->select(['id'])
                        ->where(['email' => $email]);
                    foreach ($query->all() as $result) {
                        $resend = $this->Users->get($result['id']);
                        $resend->activation_code = $newCode;
                        if ($this->Users->save($resend)) {
                            $mailer = new Mailer();
                            $mailer ->setEmailFormat('html')
                                ->setTo($user['email'])
                                ->setFrom('microblog.cake@gmail.com')
                                ->setSubject('MicroBlog Email Activation.')
                                ->viewBuilder()
                                ->setTemplate('activationCode');

                            $mailer ->setViewVars([
                                'activationCode' => $newCode,
                                'name' => $result['full_name'],
                            ])
                                ->deliver();
                            $this->Flash->success(__('Email sent.'));
                        }
                    }
                }
            } else {
                $this->Flash->error(__('Failed: Email does not exist.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * forgot password method
     *
     * @return \Cake\Http\Response|null|void Redirects to login.
     */
    public function forgotpassword()
    {
        $user = $this->Users->newEmptyEntity();
        $newpass = $this->generateRandomString();

        if ($this->getRequest()->is('post')) {
            $user = $this->Users->patchEntity($user, $this->getRequest()->getData());

            $query = $this->Users
                ->find()
                ->select([
                    'activation_code',
                    'full_name',
                    'acc_status_del',
                    'id',
                ])
                ->where(['email' => $user['email']]);
            $number = $query->count();
            if ($number != 0) {
                foreach ($query->all() as $result) {
                    if ($result['acc_status_del'] == 0) {
                        $this->Flash->error(__('Failed: Account not verified.'));
                    } elseif ($result['acc_status_del'] == 1) {
                        $currDateTime = date('Y-m-d H:i:s');
                        $forgotPass = $this->Users->get($result['id']);
                        $forgotPass->password = $newpass;
                        $forgotPass->modified = $currDateTime;

                        if ($this->Users->save($forgotPass)) {
                            $mailer = new Mailer();
                            $mailer ->setEmailFormat('html')
                                ->setTo($user['email'])
                                ->setFrom('microblog.cake@gmail.com')
                                ->setSubject('MicroBlog Temporary Password.')
                                ->viewBuilder()
                                ->setTemplate('default');

                            $mailer ->setViewVars([
                                'newPass' => $newpass,
                                'name' => $result['full_name'],
                            ])
                                ->deliver();

                            $this->Flash->success(__('Email sent.'));
                        } else {
                            $this->Flash->error(__('Something went wrong. Please, try again!'));
                        }

                        return $this->redirect(['action' => 'login']);
                    } elseif ($result['acc_status_del'] == 2) {
                        $this->Flash->error(__('Failed: Account is deactivated.'));
                    }
                }
            } else {
                $this->Flash->error(__('Failed: Email does not exist.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * timeline method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index of PostsController.
     */
    public function timeline($id = null)
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            if ($this->getRequest()->is('post')) {
                $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();

                //will be the temporary container for each array for merging
                $mergeTimeline = [];

                //all your posts
                $allPost = $this->Users->find()
                    ->select([
                        'c.id',
                        'c.user_id',
                        'c.content',
                        'c.image',
                        'c.post_status_del',
                        'c.modified',
                        'full_name',
                    ])
                    ->join([
                        'table' => 'posts',
                        'alias' => 'c',
                        'type' => 'INNER',
                        'conditions' => 'c.user_id = Users.id',
                    ])
                    ->where([
                        'AND' => [
                            'c.user_id' => $id,
                            'c.post_status_del' => 0,
                        ],
                    ])
                    ->order(['c.modified' => 'DESC']);

                $timelinePosts = [];
                foreach ($allPost as $allPosts) {
                    $mergeTimeline['post_id'] = $allPosts['c']['id'];
                    $mergeTimeline['user_id'] = $allPosts['c']['user_id'];
                    $mergeTimeline['content'] = $allPosts['c']['content'];
                    $mergeTimeline['image'] = $allPosts['c']['image'];
                    $mergeTimeline['modified'] = date(
                        'Y/m/d h:i:s',
                        strtotime($allPosts['c']['modified'] . ' +8 hours')
                    );
                    $mergeTimeline['full_name'] = $allPosts['full_name'];
                    $mergeTimeline['classification'] = 'post';
                    $mergeTimeline['repost_id'] = '';
                    $mergeTimeline['repost_user_id'] = '';
                    array_push($timelinePosts, $mergeTimeline);
                }

                //all your shared posts

                $allShared = $this->Users->find()
                    ->select([
                        'c.id',
                        'c.user_id',
                        'c.content',
                        'c.image',
                        'c.post_status_del',
                        'b.modified',
                        'full_name',
                        'b.id',
                        'b.user_id',
                        'b.repost_status_del',
                    ])
                    ->join([
                        [
                            'table' => 'posts',
                            'alias' => 'c',
                            'type' => 'INNER',
                            'conditions' => 'c.user_id = Users.id',
                        ],
                        [
                            'table' => 'reposts',
                            'alias' => 'b',
                            'type' => 'INNER',
                            'conditions' => 'c.id = b.post_id',
                        ],
                    ])
                    ->where([
                        'AND' => [
                            'b.user_id' => $id,
                            'c.post_status_del' => 0,
                            'b.repost_status_del' => 0,
                        ],
                    ])
                    ->order(['b.modified' => 'DESC']);

                $timelineShares = [];
                foreach ($allShared as $allShares) {
                    $mergeTimeline['post_id'] = $allShares['c']['id'];
                    $mergeTimeline['user_id'] = $allShares['c']['user_id'];
                    $mergeTimeline['content'] = $allShares['c']['content'];
                    $mergeTimeline['image'] = $allShares['c']['image'];
                    $mergeTimeline['modified'] = date(
                        'Y/m/d H:i:s',
                        strtotime($allShares['b']['modified'] . ' +8 hours')
                    );
                    $mergeTimeline['full_name'] = $allShares['full_name'];
                    $mergeTimeline['classification'] = 'repost';
                    $mergeTimeline['repost_id'] = $allShares['b']['id'];
                    $mergeTimeline['repost_user_id'] = $allShares['b']['user_id'];
                    array_push($timelineShares, $mergeTimeline);
                }

                //merge all posts and all shares then sort by field modified.
                //using php array functions

                $timelineMerged = array_merge($timelinePosts, $timelineShares);
                $modified = array_column($timelineMerged, 'modified');
                array_multisort($modified, SORT_DESC, $timelineMerged);

                $profile = $this->Users->find()
                    ->select([
                        'id',
                        'username',
                        'full_name',
                        'profile_pic',
                    ])
                    ->where(['id' => $id]);

                $checkFollow = $this->Users->Follows->find()
                    ->select()
                    ->where([
                        'AND' => [
                            'follower_user_id' => $uid,
                            'user_id' => $id,
                        ],
                    ]);
                $countCheckFollow = $checkFollow->count();

                //followers of visitted user, you are following except you
                //SELECT * FROM users WHERE id IN (SELECT follower_user_id FROM follows WHERE follower_user_id IN (SELECT user_id FROM follows WHERE follower_user_id = $uid) AND user_id = $id)

                $followerFollowingSub1 = $this->Users->Follows->find()
                    ->select(['user_id'])
                    ->where(['follower_user_id' => $uid]);

                $followerFollowingSub2 = $this->Users->Follows->find()
                    ->select(['follower_user_id'])
                    ->where([
                        'AND' => [
                            'follower_user_id IN' => $followerFollowingSub1,
                            'user_id = ' => $id,
                        ],
                    ]);

                $followerFollowing = $this->Users->find()
                    ->select(['id',
                        'username',
                        'full_name',
                        'profile_pic',
                        'acc_status_del',
                    ])
                    ->where(['id IN' => $followerFollowingSub2]);

                //followers of visitted user, you are not following except you
                //SELECT * FROM users WHERE id IN (SELECT follower_user_id FROM follows WHERE follower_user_id NOT IN (SELECT user_id FROM follows WHERE follower_user_id = $uid) AND user_id = $id)

                $followerUnfollowingSub1 = $this->Users->Follows->find()
                    ->select(['user_id'])
                    ->where(['follower_user_id' => $uid]);

                $followerUnfollowingSub2 = $this->Users->Follows->find()
                    ->select(['follower_user_id'])
                    ->where([
                        'AND' => [
                            'follower_user_id NOT IN' => $followerUnfollowingSub1,
                            'user_id' => $id,
                        ],
                    ]);

                $followerUnfollowing = $this->Users->find()
                    ->select([
                        'id',
                        'username',
                        'full_name',
                        'profile_pic',
                        'acc_status_del',
                    ])
                    ->where(['id IN' => $followerUnfollowingSub2]);

                //followed users of the user you visitted, you are following, except you
                //SELECT * FROM users WHERE id IN (SELECT user_id FROM follows WHERE user_id IN (SELECT user_id FROM follows WHERE follower_user_id = $uid) AND follower_user_id = $id)

                $followedFollowingSub1 = $this->Users->Follows->find()
                    ->select(['user_id'])
                    ->where(['follower_user_id' => $uid]);

                $followedFollowingSub2 = $this->Users->Follows->find()
                    ->select(['user_id'])
                    ->where([
                        'AND' => [
                            'user_id IN' => $followedFollowingSub1,
                            'follower_user_id' => $id,
                        ],
                    ]);

                $followedFollowing = $this->Users->find()
                    ->select(['id',
                        'username',
                        'full_name',
                        'profile_pic',
                        'acc_status_del',
                    ])
                    ->where(['id IN' => $followedFollowingSub2]);

                //followed users of the user you visitted, you are not following, except you
                //SELECT * FROM users WHERE id IN (SELECT user_id FROM follows WHERE user_id NOT IN (SELECT user_id FROM follows WHERE follower_user_id = $uid) AND follower_user_id = $id)

                $followedUnfollowingSub1 = $this->Users->Follows->find()
                    ->select(['user_id'])
                    ->where(['follower_user_id' => $uid]);

                $followedUnfollowingSub2 = $this->Users->Follows->find()
                    ->select(['user_id'])
                    ->where([
                        'AND' => [
                            'user_id NOT IN' => $followedUnfollowingSub1,
                            'follower_user_id' => $id,
                        ],
                    ]);

                $followedUnfollowing = $this->Users->find()
                    ->select([
                        'id',
                        'username',
                        'full_name',
                        'profile_pic',
                        'acc_status_del',
                    ])
                    ->where(['id IN' => $followedUnfollowingSub2]);

                $this->set(compact(
                    'post',
                    'users',
                    'profile',
                    'countCheckFollow',
                    'timelineMerged',
                    'followerUnfollowing',
                    'followerFollowing',
                    'followedUnfollowing',
                    'followedFollowing'
                ));
            } else {
                $this->Flash->error(__('Something went wrong. Please, try again!'));

                return $this->redirect([
                    'controller' => 'Posts',
                    'action' => 'index',
                ]);
            }
        } else {
            return $this->redirect(['action' => 'login']);
        }
    }
}
