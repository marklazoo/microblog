<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Reposts Controller
 *
 * @property \App\Model\Table\RepostsTable $Reposts
 * @method \App\Model\Entity\Repost[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RepostsController extends AppController
{
    /**
     * share method
     *
     * @param string|null $id Repost id.
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function share($id = null)
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();
            if ($this->getRequest()->is('post')) {
                $sharePost = $this->Reposts->newEmptyEntity();
                $sharePost->post_id = $id;
                $sharePost->user_id = $uid;

                if ($this->Reposts->save($sharePost)) {
                    $this->Flash->success(__('You shared this post.'));
                } else {
                    $this->Flash->error(__('Unable to share this post.Please, try again!'));
                }

                return $this->redirect([
                    'controller' => 'posts',
                    'action' => 'view',
                    $id,
                ]);
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Repost id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->status();

        $repost = $this->Reposts->findById($id)
            ->firstOrFail();
        $user_id = $repost['user_id'];

        if ($this->Authentication->getIdentity()) {
            if (!$this->isAuthor($user_id)) {
                if ($this->getRequest()->is('post')) {
                    $currDateTime = date('Y-m-d H:i:s');

                    $reposts = $this->Reposts->get($id);
                    $reposts->repost_status_del = 1;
                    $reposts->modified = $currDateTime;

                    if ($this->Reposts->save($reposts)) {
                        $this->Flash->success(__('Your shared post has been deleted.'));
                    } else {
                        $this->Flash->error(__('Unable to delete this post. Please, try again!'));
                    }

                    return $this->redirect([
                        'controller' => 'posts',
                        'action' => 'index',
                    ]);
                }
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }
}
