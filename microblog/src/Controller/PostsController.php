<?php
declare(strict_types=1);

namespace App\Controller;
/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('loggedin');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->status();
        $post = $this->Posts->newEmptyEntity();

        if ($this->Authentication->getIdentity()) {
            $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();
            $divCounter = 'home';
            $mergedUserSearch = [];
            $countSearchPostResult = 0;

            if ($this->getRequest()->is('post')) {
                $formValue = $this->getRequest()->getData('formsent');
                $postUser = $this->getRequest()->getData('user_id');
                if ($formValue == 'postbar') {
                    if (!$this->isAuthor($postUser)) {
                        $name = $this->Posts
                            ->find()
                            ->select(['id']);
                        $countName = $name->count() + 1;

                        $postData = $this->getRequest()->getData();
                        $postImage = $this->getRequest()->getData('image');
                        $name = $postImage->getClientFilename();
                        $type = $postImage->getClientMediaType();
                        if (!empty($name)) {
                            if ($type == 'image/jpeg' || $type == 'image/jpg' || $type == 'image/png') {
                                $ext = substr(strtolower(strrchr($name, '.')), 1);
                                $newName = $countName . '.' . $ext;
                                $targetPath = WWW_ROOT . 'img' . DS . 'post_image' . DS . $newName;
                                if ($postImage->getSize() > 0 && $postImage->getError() == 0) {
                                    $postImage->moveTo($targetPath);
                                    $postData['image'] = $newName;
                                }
                            } else {
                                $this->Flash->error(
                                    __('Failed Save: Incorrect image extension. Use jpeg, jpg, & png.')
                                );

                                return $this->redirect(['action' => 'index']);
                            }
                        } else {
                            $postData['image'] = null;
                        }

                        $posts = $this->Posts->patchEntity($post, $postData);
                        if ($this->Posts->save($posts)) {
                            $this->Flash->success(__('The post has been saved.'));

                            return $this->redirect(['action' => 'index']);
                        }
                        $this->Flash->error(__(' Failed Save: Please, try again.'));
                    }
                } else {
                    $searchValue = $this->getRequest()->getData('search');
                    $searchPostResult = $this->Posts
                        ->find()
                        ->select()
                        ->where([
                            'AND' => [
                                'content LIKE' => '%' . $searchValue . '%',
                                'post_status_del' => 0,
                            ],
                        ])
                        ->order(['modified' => 'DESC']);

                    $countSearchPostResult = $searchPostResult->count();

                    //users who matched the keyword,  not followed

                    $toFollowSubquerySearch = $this->Posts->Users->Follows
                        ->find()
                        ->select(['user_id'])
                        ->where(['follower_user_id' => $uid]);

                    $toFollowSearch = $this->Posts->Users
                        ->find()
                        ->select([
                            'id',
                            'username',
                            'full_name',
                            'profile_pic',
                        ])
                        ->where([
                            'AND' => [
                                'id NOT IN' => $toFollowSubquerySearch,
                                'id !=' => $uid, 'acc_status_del' => 1,
                                'OR' => [
                                    'username LIKE' => '%' . $searchValue . '%',
                                    'full_name LIKE' => '%' . $searchValue . '%',
                                ],
                            ],
                        ]);

                    $notFollowed = [];
                    $mergeFollow = [];

                    foreach ($toFollowSearch as $toFollowSearches) {
                        $mergeFollow['user_id'] = $toFollowSearches['id'];
                        $mergeFollow['username'] = $toFollowSearches['username'];
                        $mergeFollow['full_name'] = $toFollowSearches['full_name'];
                        $mergeFollow['profile_pic'] = $toFollowSearches['profile_pic'];
                        $mergeFollow['status'] = 'no';
                        array_push($notFollowed, $mergeFollow);
                    }

                    //users who matched the keyword, followed

                    $followingSearch = $this->Posts->Users
                        ->find()
                        ->select([
                            'id',
                            'username',
                            'full_name',
                            'profile_pic',
                        ])
                        ->join([
                            'table' => 'follows',
                            'alias' => 'c',
                            'type' => 'INNER',
                            'conditions' => 'c.user_id = Users.id',
                        ])
                        ->where([
                            'AND' => [
                                'c.follower_user_id' => $uid,
                                'acc_status_del' => 1,
                                'OR' => [
                                    'username LIKE' => '%' . $searchValue . '%',
                                    'full_name LIKE' => '%' . $searchValue . '%',
                                ],
                            ],
                        ]);

                    $followed = [];
                    $mergeFollow = [];

                    foreach ($followingSearch as $followingSearches) {
                        $mergeFollow['user_id'] = $followingSearches['id'];
                        $mergeFollow['username'] = $followingSearches['username'];
                        $mergeFollow['full_name'] = $followingSearches['full_name'];
                        $mergeFollow['profile_pic'] = $followingSearches['profile_pic'];
                        $mergeFollow['status'] = 'yes';
                        array_push($followed, $mergeFollow);
                    }

                    //merge followed and not followed users

                    $mergedUserSearch = array_merge($notFollowed, $followed);
                    $username = array_column($mergedUserSearch, 'username');
                    array_multisort($username, SORT_DESC, $mergedUserSearch);

                    $divCounter = 'search';
                }
            }

            //all your and your followed users posts with join to show who posted it

            $allPostSubquery = $this->Posts->Users->Follows
                ->find()
                ->select(['user_id'])
                ->where(['follower_user_id' => $uid]);

            $allPost = $this->Posts->Users
                ->find()
                ->select([
                    'c.id',
                    'c.user_id',
                    'c.content',
                    'c.image',
                    'c.post_status_del',
                    'c.modified',
                    'full_name',
                ])
                ->join([
                    'table' => 'posts',
                    'alias' => 'c',
                    'type' => 'INNER',
                    'conditions' => 'c.user_id = Users.id',
                ])
                ->where([
                    'AND' => [
                        'OR' => [
                            'c.user_id IN' => $allPostSubquery,
                            'c.user_id' => $uid,
                        ],
                        'c.post_status_del' => 0,
                    ],
                ])
                ->order(['c.modified' => 'DESC']);

            $posts = [];
            $mergeCounter = [];

            foreach ($allPost as $allPosts) {
                $mergeCounter['post_id'] = $allPosts['c']['id'];
                $mergeCounter['user_id'] = $allPosts['c']['user_id'];
                $mergeCounter['content'] = $allPosts['c']['content'];
                $mergeCounter['image'] = $allPosts['c']['image'];
                $mergeCounter['modified'] = date('Y/m/d H:i:s', strtotime($allPosts['c']['modified'] . ' +8 hours'));
                $mergeCounter['full_name'] = $allPosts['full_name'];
                $mergeCounter['classification'] = 'post';
                $mergeCounter['repost_id'] = '';
                $mergeCounter['repost_user_id'] = '';
                array_push($posts, $mergeCounter);
            }

            //all your and your followed users shared posts

            $allSharedSubquery = $this->Posts->Users->Follows
                ->find()
                ->select(['user_id'])
                ->where(['follower_user_id' => $uid]);

            $allShared = $this->Posts->Users
                ->find()
                ->select([
                    'c.id',
                    'c.user_id',
                    'c.content',
                    'c.image',
                    'c.post_status_del',
                    'b.modified',
                    'full_name',
                    'b.id',
                    'b.user_id',
                    'b.repost_status_del',
                ])
                ->join([
                    [
                        'table' => 'reposts',
                        'alias' => 'b',
                        'type' => 'INNER',
                        'conditions' => 'b.user_id = Users.id',
                    ],
                    [
                        'table' => 'posts',
                        'alias' => 'c',
                        'type' => 'INNER',
                        'conditions' => 'c.id = b.post_id',
                    ],
                ])
                ->where([
                    'AND' => [
                        'OR' => [
                            'b.user_id IN' => $allSharedSubquery,
                            'b.user_id' => $uid,
                        ],
                        'c.post_status_del' => 0,
                        'b.repost_status_del' => 0,
                    ],
                ])
                ->order(['b.modified' => 'DESC']);

            $shares = [];
            foreach ($allShared as $allShares) {
                $mergeCounter['post_id'] = $allShares['c']['id'];
                $mergeCounter['user_id'] = $allShares['c']['user_id'];
                $mergeCounter['content'] = $allShares['c']['content'];
                $mergeCounter['image'] = $allShares['c']['image'];
                $mergeCounter['modified'] = date('Y/m/d H:i:s', strtotime($allShares['b']['modified'] . ' +8 hours'));
                $mergeCounter['full_name'] = $allShares['full_name'];
                $mergeCounter['classification'] = 'repost';
                $mergeCounter['repost_id'] = $allShares['b']['id'];
                $mergeCounter['repost_user_id'] = $allShares['b']['user_id'];
                array_push($shares, $mergeCounter);
            }

            //merge all posts and all shareds then sort by field modified.
            //using php array functions

            $merged = array_merge($posts, $shares);
            $modified = array_column($merged, 'modified');
            array_multisort($modified, SORT_DESC, $merged);

            //users you are following

            $follows = $this->Posts->Users
                ->find()
                ->select([
                    'id',
                    'username',
                    'full_name',
                    'profile_pic',
                    'acc_status_del',
                ])
                ->join([
                    'table' => 'follows',
                    'alias' => 'c',
                    'type' => 'INNER',
                    'conditions' => 'c.user_id = Users.id',
                ])
                ->where(['c.follower_user_id' => $uid]);

            //users you are not following
            //SELECT * FROM users WHERE id NOT IN (SELECT user_id FROM follows WHERE follower_user_id = 2) AND id != 2;

            $toFollowSubquery = $this->Posts->Users->Follows
                ->find()
                ->select(['user_id'])
                ->where(['follower_user_id' => $uid]);

            $toFollow = $this->Posts->Users
                ->find()
                ->select([
                    'id',
                    'username',
                    'full_name',
                    'profile_pic',
                ])
                ->where([
                    'AND' => [
                        'id NOT IN' => $toFollowSubquery,
                        'id !=' => $uid,
                        'acc_status_del' => 1,
                    ],
                ]);

            //followers you are following
            //SELECT * FROM users WHERE id IN (SELECT user_id FROM follows WHERE user_id IN (SELECT follower_user_id FROM follows WHERE user_id = 4) AND follower_user_id = 4)

            $followerFollowingSub1 = $this->Posts->Users->Follows
                ->find()
                ->select(['follower_user_id'])
                ->where(['user_id' => $uid]);

            $followerFollowingSub2 = $this->Posts->Users->Follows
                ->find()
                ->select(['user_id'])
                ->where([
                    'AND' => [
                        'user_id IN' => $followerFollowingSub1,
                        'follower_user_id' => $uid,
                    ],
                ]);

            $followerFollowing = $this->Posts->Users
                ->find()
                ->select([
                    'id',
                    'username',
                    'full_name',
                    'profile_pic',
                    'acc_status_del',
                ])
                ->where(['id IN' => $followerFollowingSub2]);

            //followers you are not following
            //SELECT * FROM users WHERE id IN (SELECT follower_user_id FROM follows WHERE follower_user_id NOT IN (SELECT user_id FROM follows WHERE follower_user_id = 4) AND user_id = 4)

            $followerUnfollowingSub1 = $this->Posts->Users->Follows
                ->find()
                ->select(['user_id'])
                ->where(['follower_user_id' => $uid]);

            $followerUnfollowingSub2 = $this->Posts->Users->Follows
                ->find()
                ->select(['follower_user_id'])
                ->where([
                    'AND' => [
                        'follower_user_id NOT IN' => $followerUnfollowingSub1,
                        'user_id' => $uid,
                    ],
                ]);

            $followerUnfollowing = $this->Posts->Users
                ->find()
                ->select([
                    'id',
                    'username',
                    'full_name',
                    'profile_pic',
                    'acc_status_del',
                ])
                ->where(['id IN' => $followerUnfollowingSub2]);

            $profile = $this->Posts->Users
                ->find()
                ->select([
                    'id',
                    'username',
                    'full_name',
                    'profile_pic',
                ])
                ->where(['id' => $uid]);

            $this->set(compact(
                'post',
                'users',
                'follows',
                'toFollow',
                'followerFollowing',
                'followerUnfollowing',
                'profile',
                'searchPostResult',
                'divCounter',
                'countSearchPostResult',
                'merged',
                'mergedUserSearch'
            ));
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->status();
        if ($this->Authentication->getIdentity()) {
            $uid = $this->getRequest()->getAttribute('identity')->getIdentifier();
            $post = $this->Posts->get($id, [
                'contain' => [
                    'Users',
                    'Comments',
                    'Likes',
                    'Reposts',
                ],
            ]);

            $posterInfo = $this->Posts->Users
                ->find()
                ->select([
                    'id',
                    'username',
                    'full_name',
                    'profile_pic',
                    'c.user_id',
                ])
                ->join([
                    'table' => 'posts',
                    'alias' => 'c',
                    'type' => 'INNER',
                    'conditions' => 'c.user_id = Users.id',
                ])
                ->where(['c.id' => $id]);

            $commentInfo = $this->Posts->Users
                ->find()
                ->select([
                    'id',
                    'username',
                    'full_name',
                    'profile_pic',
                    'c.comment_text',
                    'c.id',
                    'c.comment_status_del',
                    'c.modified',
                ])
                ->join([
                    'table' => 'comments',
                    'alias' => 'c',
                    'type' => 'INNER',
                    'conditions' => 'c.user_id = Users.id',
                ])
                ->where([
                    'AND' => [
                        'c.post_id' => $id,
                        'c.comment_status_del' => 0,
                    ],
                ])
                ->order(['c.modified' => 'DESC']);
            $countComments = $commentInfo->count();

            $repostInfo = $this->Posts->Users
                ->find()
                ->select([
                    'id',
                    'username',
                    'full_name',
                    'profile_pic',
                    'c.created',
                    'c.repost_status_del',
                ])
                ->join([
                    'table' => 'reposts',
                    'alias' => 'c',
                    'type' => 'INNER',
                    'conditions' => 'c.user_id = Users.id',
                ])
                ->where([
                    'AND' => [
                        'c.post_id' => $id,
                        'c.repost_status_del' => 0,
                    ],
                ])
                ->order(['c.modified' => 'DESC']);
            $countShares = $repostInfo->count();

            $listLiker = $this->Posts->Users
                ->find()
                ->select([
                    'id',
                    'username',
                    'full_name',
                    'profile_pic',
                    'c.created',
                ])
                ->join([
                    'table' => 'likes',
                    'alias' => 'c',
                    'type' => 'INNER',
                    'conditions' => 'c.user_id = Users.id',
                ])
                ->where(['c.post_id' => $id])
                ->order(['c.modified' => 'DESC']);
            $countTotal = $listLiker->count();

            $likedInfo = $this->Posts->Likes
                ->find()
                ->select(['post_id'])
                ->where([
                    'AND' => [
                        'user_id' => $uid,
                        'post_id' => $id,
                    ],
                ]);
            $count = $likedInfo->count();

            $this->set(compact(
                'post',
                'posterInfo',
                'count',
                'commentInfo',
                'repostInfo',
                'countTotal',
                'countComments',
                'countShares',
                'listLiker'
            ));
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $post = $this->Posts->newEmptyEntity();

            if ($this->getRequest()->is('post')) {
                $post = $this->Posts->patchEntity($post, $this->getRequest()->getData());
                if ($this->Posts->save($post)) {
                    $this->Flash->success(__('The post has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('Failed Save: Please, try again.'));
            }
            $users = $this->Posts->Users->find(
                'list',
                ['limit' => 200]
            );
            $this->set(compact('post', 'users'));
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->status();

        if ($this->Authentication->getIdentity()) {
            $post = $this->Posts
                ->findById($id)
                ->firstOrFail();

            $countName = $post['id'];

            $post = $this->Posts->get(
                $id,
                ['contain' => [],
                ]
            );

            if (!$this->isAuthor($post['user_id'])) {
                if ($this->getRequest()->is('put')) {
                    $post = $this->Posts->newEmptyEntity();

                    $postData = $this->getRequest()->getData();
                    $postImage = $this->getRequest()->getData('image');
                    $postContent = $this->getRequest()->getData('content');
                    $name = $postImage->getClientFilename();
                    $type = $postImage->getClientMediaType();
                    if (!empty($name)) {
                        if ($type == 'image/jpeg' || $type == 'image/jpg' || $type == 'image/png') {
                            $ext = substr(strtolower(strrchr($name, '.')), 1);
                            $newName = $countName . '.' . $ext;
                            $targetPath = WWW_ROOT . 'img' . DS . 'post_image' . DS . $newName;
                            if ($postImage->getSize() > 0 && $postImage->getError() == 0) {
                                $postImage->moveTo($targetPath);
                                $postData['image'] = $newName;
                            }
                        } else {
                            $this->Flash->error(__('Failed Save: Incorrect image extension. Use jpeg, jpg, & png.'));

                            return $this->redirect([
                                'action' => 'edit',
                                $id,
                            ]);
                        }
                    } else {
                        $postData['image'] = null;
                    }

                    $currDateTime = date('Y-m-d H:i:s');
                    $editPost = $this->Posts->get($id);
                    $query = $this->Posts
                        ->find()
                        ->select(['id'])
                        ->where(['id' => $id]);
                    foreach ($query->all() as $result) {
                        if ($postData['image'] != null) {
                            $editPost->content = $postContent;
                            $editPost->modified = $currDateTime;
                            $editPost->image = $postData['image'];
                        } else {
                            $editPost->content = $postContent;
                            $editPost->modified = $currDateTime;
                        }

                        if ($this->Posts->save($editPost)) {
                            $this->Flash->success(__('Changes saved successfully.'));
                        } else {
                            $this->Flash->error(__('Unable to save changes. Please, try again!'));
                        }

                        return $this->redirect([
                            'action' => 'view',
                            $result['id'],
                        ]);
                    }
                }
            }

            $users = $this->Posts->Users->find(
                'list',
                ['limit' => 200]
            );
            $this->set(compact(
                'post',
                'users'
            ));
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->status();

        $deletePost = $this->Posts->get($id);

        if ($this->Authentication->getIdentity()) {
            if (!$this->isAuthor($deletePost->user_id)) {
                if ($this->getRequest()->is('post')) {
                    $currDateTime = date('Y-m-d H:i:s');

                    $deletePost->post_status_del = 1;
                    $deletePost->modified = $currDateTime;

                    if ($this->Posts->save($deletePost)) {
                        $this->Flash->success(__('The post has been deleted.'));
                    } else {
                        $this->Flash->error(__('Unable to delete this post. Please, try again!'));
                    }

                    return $this->redirect(['action' => 'index']);
                }
            }
        } else {
            return $this->redirect([
                'controller' => 'users',
                'action' => 'login',
            ]);
        }
    }
}
