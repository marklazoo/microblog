let currentItems = 4;
let currentItemsTimeline = 6;
let currentItemsComment = 3;
let currentItemsShare = 3;
let currentItemsLike = 3;
let currentItemsUserSearch = 6;
let currentItemsPostSearch = 3;

function loadmore()
{
    const elementList = [...document.querySelectorAll('.list .list-element')];
    for (let i = currentItems; i < currentItems + 4; i++) {
        if (elementList[i]) {
            elementList[i].style.display = 'block';
        }
    }
    currentItems += 4;

    // Load more button will be hidden after list fully loaded
    if (currentItems >= elementList.length) {
        event.target.style.display = 'none';
    }
}

function loadmoreTimeline()
{
    const elementListTimeline = [...document.querySelectorAll('.list-timeline .list-timeline-element')];
    for (let i = currentItemsTimeline; i < currentItemsTimeline + 6; i++) {
        if (elementListTimeline[i]) {
            elementListTimeline[i].style.display = 'block';
        }
    }
    currentItemsTimeline += 6;

    // Load more button will be hidden after list fully loaded
    if (currentItemsTimeline >= elementListTimeline.length) {
        event.target.style.display = 'none';
    }
}

function loadmoreComment()
{
    const elementListComment = [...document.querySelectorAll('.list-comment .list-comment-element')];
    for (let i = currentItemsComment; i < currentItemsComment + 3; i++) {
        if (elementListComment[i]) {
            elementListComment[i].style.display = 'block';
        }
    }
    currentItemsComment += 3;

    // Load more button will be hidden after list fully loaded
    if (currentItemsComment >= elementListComment.length) {
        event.target.style.display = 'none';
    }
}

function loadmoreUserSearch()
{
    const elementListUserSearch = [...document.querySelectorAll('.list-UserSearch .list-UserSearch-element')];
    for (let i = currentItemsUserSearch; i < currentItemsUserSearch + 6; i++) {
        if (elementListUserSearch[i]) {
            elementListUserSearch[i].style.display = 'block';
        }
    }
    currentItemsUserSearch += 6;

    // Load more button will be hidden after list fully loaded
    if (currentItemsUserSearch >= elementListUserSearch.length) {
        event.target.style.display = 'none';
    }
}

function loadmorePostSearch()
{
    const elementListPostSearch = [...document.querySelectorAll('.list-PostSearch .list-PostSearch-element')];
    for (let i = currentItemsPostSearch; i < currentItemsPostSearch + 3; i++) {
        if (elementListPostSearch[i]) {
            elementListPostSearch[i].style.display = 'block';
        }
    }
    currentItemsPostSearch += 3;

    // Load more button will be hidden after list fully loaded
    if (currentItemsPostSearch >= elementListPostSearch.length) {
        event.target.style.display = 'none';
    }
}

//toggle button for showing list of likers or list of sharers

function likes()
{
    var y = document.getElementById("likes");
    y.style.display = "block";
    var x = document.getElementById("shares");
    x.style.display = "none";
}

function shares()
{
    var x = document.getElementById("shares");
    x.style.display = "block";
    var y = document.getElementById("likes");
    y.style.display = "none";
}

//toggle add comment and edit comment forms

function editComment(comment_text, post_id, comment_id)
{
    var comment = document.getElementById("comment_text_edit");
    document.getElementById("addComment").style.display = "none";
    document.getElementById("editComment").style.display = "block";
    comment.value = comment_text;
    document.getElementById("comment_id").value = comment_id;
    document.getElementById('countEdit').innerHTML = (140 - comment.value.length) + '/140';
}

function cancel()
{
    document.getElementById("editComment").style.display = "none";
    document.getElementById("addComment").style.display = "block";
}