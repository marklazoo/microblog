const loadmoreTimeline = document.querySelector('#loadmoreTimeline');
let currentItemsTimeline = 6;
loadmoreTimeline.addEventListener('click', (e) => {
    const elementListTimeline = [...document.querySelectorAll('.list-timeline .list-timeline-element')];
    for (let i = currentItemsTimeline; i < currentItemsTimeline + 6; i++) {
        if (elementListTimeline[i]) {
            elementListTimeline[i].style.display = 'block';
        }
    }
    currentItemsTimeline += 6;

    // Load more button will be hidden after list fully loaded
    if (currentItemsTimeline >= elementListTimeline.length) {
        event.target.style.display = 'none';
    }
})