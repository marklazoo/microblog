<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RepostsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RepostsTable Test Case
 */
class RepostsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RepostsTable
     */
    protected $Reposts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Reposts',
        'app.Users',
        'app.Posts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Reposts') ? [] : ['className' => RepostsTable::class];
        $this->Reposts = $this->getTableLocator()->get('Reposts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Reposts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
