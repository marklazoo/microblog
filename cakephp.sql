-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2021 at 05:42 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cakephp`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment_text` varchar(255) NOT NULL,
  `comment_status_del` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `comment_text`, `comment_status_del`, `created`, `modified`) VALUES
(1, 1, 1, 'I removed some actions in the comment. I am trying to see if there are errors now. Thank you.', 0, '2021-05-06 02:12:22', '2021-05-06 02:12:22'),
(2, 1, 1, 'This is for comment editing. Status: Edited. Thank you.', 0, '2021-05-06 02:12:49', '2021-05-06 02:13:35'),
(3, 1, 1, 'I will be deleted.', 1, '2021-05-06 02:13:51', '2021-05-06 02:13:56');

-- --------------------------------------------------------

--
-- Table structure for table `follows`
--

CREATE TABLE `follows` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `follower_user_id` int(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `follows`
--

INSERT INTO `follows` (`id`, `user_id`, `follower_user_id`, `created`, `modified`) VALUES
(2, 1, 3, '2021-04-29 05:51:28', '2021-04-29 05:51:28'),
(3, 2, 3, '2021-04-29 05:51:33', '2021-04-29 05:51:33'),
(5, 1, 4, '2021-04-29 09:31:20', '2021-04-29 09:31:20'),
(23, 3, 1, '2021-05-03 05:49:08', '2021-05-03 05:49:08'),
(34, 1, 2, '2021-05-05 05:49:44', '2021-05-05 05:49:44'),
(38, 5, 2, '2021-05-05 05:55:09', '2021-05-05 05:55:09'),
(39, 4, 2, '2021-05-05 05:55:20', '2021-05-05 05:55:20'),
(45, 2, 1, '2021-05-06 02:17:02', '2021-05-06 02:17:02');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `post_id` int(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `post_id`, `created`, `modified`) VALUES
(2, 1, 1, '2021-05-06 02:18:16', '2021-05-06 02:18:16'),
(3, 2, 1, '2021-05-06 02:18:36', '2021-05-06 02:18:36'),
(4, 1, 2, '2021-05-06 02:24:21', '2021-05-06 02:24:21');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` varchar(140) NOT NULL,
  `image` varchar(255) NOT NULL,
  `post_status_del` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `content`, `image`, `post_status_del`, `created`, `modified`) VALUES
(1, 1, 'I am trying to remove unused actions in each controller. This is for that purpose, testing if something goes wrong after removals. Thanks.', '1.jpg', 0, '2021-05-06 02:11:33', '2021-05-06 02:11:33'),
(2, 1, 'I removed a method in PostsController, this is to see if there is an error. Thank you.', '', 0, '2021-05-06 02:22:53', '2021-05-06 02:22:53');

-- --------------------------------------------------------

--
-- Table structure for table `reposts`
--

CREATE TABLE `reposts` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `post_id` int(20) NOT NULL,
  `repost_status_del` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reposts`
--

INSERT INTO `reposts` (`id`, `user_id`, `post_id`, `repost_status_del`, `created`, `modified`) VALUES
(1, 1, 1, 0, '2021-05-06 02:18:08', '2021-05-06 02:18:08'),
(2, 2, 1, 0, '2021-05-06 02:18:42', '2021-05-06 02:18:42'),
(3, 1, 2, 1, '2021-05-06 02:24:19', '2021-05-06 02:24:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `activation_code` varchar(255) NOT NULL,
  `email_activated` datetime NOT NULL,
  `acc_status_del` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `full_name`, `birthdate`, `email`, `password`, `profile_pic`, `activation_code`, `email_activated`, `acc_status_del`, `created`, `modified`) VALUES
(1, '_trial1', 'First Trial', '2021-04-04', 'trial1@gmail.com', '$2y$10$Wr2Wl2JpffrpvbA5lokii.U2MJH2K6eHawyNNd5fIVoRWcdBz0WzO', '1.jpg', '4NNQ2cU9g4', '2021-05-03 05:54:58', 1, '2021-04-29 02:59:08', '2021-05-06 03:15:27'),
(2, '_trial2', 'Second Trial', '2021-04-29', 'trial2@gmail.com', '$2y$10$r2KYMEo/OPE7Y4ZDre6dGOdowlZ28KdlzpV0wymtAywCex4WtZk/2', '2.jpg', 'lJBp7qD2oY', '2021-04-29 03:19:26', 1, '2021-04-29 03:19:07', '2021-04-29 05:12:59'),
(3, '_trial3', 'Third Trial', '2021-04-29', 'trial3@gmail.com', '$2y$10$j.GnQ7ffPupgcgKj2oQjteu8V9DyPAa9G37uQEc9G83G/Kzv1KwSK', '', 'HmzkDqUcPs', '2021-05-03 03:21:41', 0, '2021-04-29 05:51:00', '2021-04-29 06:25:59'),
(4, '_trial4', 'Fourth Trial', '2021-04-29', 'trial4@gmail.com', '$2y$10$5LD5oXzxxk/ruXY5lfJ6Nuf1KH8UNC2PLOKFYFV54Mh1HsrwoWcPy', '', 'gKH7a8bLFx', '2021-04-29 09:28:43', 1, '2021-04-29 09:28:12', '2021-04-30 05:58:28'),
(5, '_trial5', 'Fifth Trial', '2021-04-29', 'trial5@gmail.com', '$2y$10$npK1tTYUc9L4fW1XKr4tY.lh.aa6lFN3nB5M0F6D9au0Aeqhi0Zl2', '', 'FM9Fri6Tal', '2021-04-29 12:17:22', 1, '2021-04-29 12:12:35', '2021-04-30 05:44:17'),
(6, '_trial6', 'Sixth Trial', '2021-04-30', 'trial6@gmail.com', '$2y$10$2FtSAwcBYgUN8QZIcUAkaeR6Hk4FJ5nlY.xmCgnXpImJr8Yo5jiVC', '', 'OFjQsaSSDP', '2021-05-04 04:55:25', 1, '2021-04-30 05:59:02', '2021-05-04 04:57:41'),
(7, '_trial7', 'Seventh Trial', '2021-05-02', 'trial7@gmail.com', '$2y$10$fMLMHXDUKW3jXFd5sApvMOwSRliq79o1KomNyKT508e67fSOD2G4O', '', 'NQWXK8Gt23', '2021-05-02 06:45:04', 1, '2021-05-02 06:44:28', '2021-05-02 06:44:28'),
(8, '_trial8', 'Eighth Trial', '2021-05-02', 'trial8@gmail.com', '$2y$10$42aQfxgjVmPWeqvY/c4Zg.Psj9HxltzupVTh9d4Oros5FvOUySAKK', '', '6iyrtGaXBN', '2021-05-02 06:46:22', 1, '2021-05-02 06:45:57', '2021-05-02 06:45:57'),
(9, '_trial9', 'Ninth Trial', '2010-03-04', 'trial9@gmail.com', '$2y$10$ynYd2VgGAPAcd5HvmR5MSu.usxzrI5Xr2cYeU0Azb0RELdHtnCJA.', '', 'rbbYoFr5gv', '2021-05-04 05:12:09', 1, '2021-05-04 05:04:38', '2021-05-04 05:08:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follows`
--
ALTER TABLE `follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reposts`
--
ALTER TABLE `reposts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `follows`
--
ALTER TABLE `follows`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reposts`
--
ALTER TABLE `reposts`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
